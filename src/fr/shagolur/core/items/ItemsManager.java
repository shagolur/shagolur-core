package fr.shagolur.core.items;

import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.StorableManager;
import fr.shagolur.core.storage.common.ItemDataPathProvider;

public class ItemsManager extends StorableManager<ItemData> {
	
	@Override
	protected PathProvider<ItemData> getProvider() {
		return new ItemDataPathProvider();
	}
	
	@Override
	protected String getStoredTypeName() {
		return "items";
	}
	
	public boolean has(String id) {
		return collection.stream().anyMatch(data -> data.getId().equals(id));
	}
	
	public void save(ItemData item) {
		storage.saveUnique(ItemData.class, item);
	}
	
}
