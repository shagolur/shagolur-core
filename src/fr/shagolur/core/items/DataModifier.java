package fr.shagolur.core.items;

import com.google.gson.annotations.SerializedName;
import fr.shagolur.core.Shagolur;
import org.bukkit.ChatColor;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

public record DataModifier(Data data, Modifier modifier, double value) {
	
	public static Set<DataModifier> flatten(Collection<Set<DataModifier>> modifiers) {
		Set<DataModifier> flat = new HashSet<>();
		Map<Data, Map<Modifier, Double>> map = new HashMap<>();
		for(Set<DataModifier> set : modifiers) {
			for(DataModifier modifier : set) {
				if(!map.containsKey(modifier.data))
					map.put(modifier.data, new HashMap<>());
				map.get(modifier.data).compute(modifier.modifier, (_modifier, oldVal) -> oldVal == null ? modifier.value : (_modifier==Modifier.ADD_FLAT) ? oldVal+modifier.value : oldVal*modifier.value);
			}
		}
		for(Map.Entry<Data, Map<Modifier, Double>> entry : map.entrySet()) {
			for(Map.Entry<Modifier, Double> mf : entry.getValue().entrySet()) {
				flat.add(new DataModifier(entry.getKey(), mf.getKey(), mf.getValue()));
			}
		}
		return flat;
	}
	
	@Override
	public String toString() {
		return "DataModifier{" + data + ", " + modifier + ", " + value + '}';
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		DataModifier that = (DataModifier) o;
		return Double.compare(that.value, value) == 0 && data == that.data && modifier == that.modifier;
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(data, modifier, value);
	}
	
	public String toLoreLine() {
		boolean positive = (modifier==Modifier.ADD_FLAT&&value>0)||(modifier==Modifier.MUL_FLAT&&value>1);
		ChatColor col = positive ? ChatColor.BLUE : ChatColor.RED;
		String val = modifier==Modifier.ADD_FLAT?value+"":String.format("%d", (int) (value*100.0))+"%";
		return col + (positive?"+":"-") + " " + val + " " + data.description;
	}
	
	public DataModifierSerializer serialize() {
		return new DataModifierSerializer(this);
	}
	
	public static class DataModifierSerializer {
		
		public String stat; // Data_name
		@SerializedName("modifier_type") public String modifier;
		public double amount;
		
		public DataModifierSerializer() {}
		public DataModifierSerializer(DataModifier dm) {
			stat = dm.data.serialize();
			modifier = dm.modifier.serialize();
			amount = dm.value;
		}
		
		public DataModifier toModifier(String idItem) {
			Data data = Data.deserialize(stat);
			if(data == null) {
				Shagolur.error("Item [id="+idItem+"] has invalid data name : \""+stat+"\".");
				data = Data.BONUS_HEALTH;
			}
			
			Modifier modifier = Modifier.deserialize(this.modifier);
			if(modifier == null) {
				Shagolur.error("Item [id="+idItem+"] has invalid modifier name : \""+this.modifier+"\".");
				modifier = Modifier.ADD_FLAT;
			}
			
			return new DataModifier(data, modifier, amount);
		}
		
		@Override
		public String toString() {
			return "DataModifierSerializer{" +
					"stat='" + stat + '\'' +
					", modifier='" + modifier + '\'' +
					", amount=" + amount +
					'}';
		}
	}
	
}
