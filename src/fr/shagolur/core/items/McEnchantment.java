package fr.shagolur.core.items;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;

public record McEnchantment(Enchantment enchantment, int level) {
	
	public String toLoreLine() {
		return ChatColor.BLUE + enchantment.toString() + " " + getRomanNumber();
	}
	
	private String getRomanNumber() {
		return "I".repeat(level)
				.replace("IIIII", "V")
				.replace("IIII", "IV")
				.replace("VV", "X")
				.replace("VIV", "IX")
				.replace("XXXXX", "L")
				.replace("XXXX", "XL")
				.replace("LL", "C")
				.replace("LXL", "XC")
				.replace("CCCCC", "D")
				.replace("CCCC", "CD")
				.replace("DD", "M")
				.replace("DCD", "CM");
	}
	
}
