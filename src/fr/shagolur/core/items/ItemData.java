package fr.shagolur.core.items;

import com.google.gson.annotations.SerializedName;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.core.storage.Storable;
import fr.shagolur.core.ui.ItemSerializable;
import fr.shagolur.core.utils.ItemBuilder;
import fr.shagolur.core.utils.SerializationUtils;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ItemData implements Storable, ItemSerializable {
	
	public static NamespacedKey key() {
		return Shagolur.key("item-id");
	}
	
	private final String id;
	private final Material type;
	private final boolean magical, droppable;
	private final Set<DataModifier> modifiers = new HashSet<>();
	private final Rarity rarity;
	private final String name;
	private final List<String> lore = new ArrayList<>();
	private final Set<McEnchantment> enchants = new HashSet<>();
	
	public ItemData(String id, Material type, boolean magical, boolean droppable, Rarity rarity, String name) {
		this.id = id;
		this.type = type;
		this.magical = magical;
		this.droppable = droppable;
		this.rarity = rarity;
		this.name = name;
	}
	
	public ItemData(String id, Material type, boolean magical, boolean droppable, Rarity rarity, String name, Set<DataModifier> modifiers, List<String> lore, Set<McEnchantment> enchants) {
		this(id, type, magical, droppable, rarity, name);
		this.modifiers.addAll(modifiers);
		this.lore.addAll(lore);
		this.enchants.addAll(enchants);
	}
	
	public static boolean hasData(@Nullable ItemStack item) {
		if(item == null || !item.hasItemMeta())
			return false;
		return item.getItemMeta().getPersistentDataContainer().has(key());
	}
	public static @Nullable ItemData convert(@Nullable ItemStack item) {
		if(!hasData(item))
			return null;
		String id = item.getItemMeta().getPersistentDataContainer().get(key(), PersistentDataType.STRING);
		return Shagolur.getItemsManager().get(id);
	}
	
	public static @Nullable String convertViaID(@Nullable ItemStack item) {
		if(item == null || !item.hasItemMeta())
			return null;
		return item.getItemMeta().getPersistentDataContainer().get(key(), PersistentDataType.STRING);
	}
	
	public boolean isDroppable() {
		return droppable;
	}
	
	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public String getDisplayName() {
		return rarity.color + name;
	}
	
	public boolean isMagical() {
		return magical;
	}
	
	public List<String> getLore() {
		return lore;
	}
	
	public Rarity getRarity() {
		return rarity;
	}
	
	public Material getType() {
		return type;
	}
	
	public Set<DataModifier> getModifiers() {
		return modifiers;
	}
	
	public Set<McEnchantment> getEnchants() {
		return enchants;
	}
	
	public Stream<DataModifier> toModiferStream() {
		return modifiers.stream();
	}
	
	@Override
	public ItemStack toItemStack(int amount) {
		ItemBuilder b = new ItemBuilder(type, amount)
				.setPersistentData(key(), PersistentDataType.STRING, id)
				.setDisplayName(rarity.color + name)
				.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_DYE, ItemFlag.HIDE_ENCHANTS, ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_UNBREAKABLE)
				.setLore(lore);
		if(!lore.isEmpty() && !modifiers.isEmpty())
			b.addLoreLine("");
		b.addLoreLines(modifiers.stream().map(DataModifier::toLoreLine).toList());
//		b.setEnchants(enchants.stream().collect(Collectors.toMap(McEnchantment::enchantment, McEnchantment::level)));
		return b.build();
	}
	
	@Override
	public Object storageID() {
		return id;
	}
	
	@Override
	public Serializer<?> serialize() {
		return new ItemDataBDD(this);
	}
	
	@Override
	public boolean alreadyInBDD() {
		return true;
	}
	
	@Override
	public void declarePersistentInBDD() {
		Shagolur.error("No way to create items from game.");
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(!(obj instanceof ItemData))
			return false;
		return ((ItemData)obj).id.equals(id);
	}
	
	@Override
	public String toString() {
		return "ItemData("+id+", name="+name+")";
	}
	
	public String stringStats(String color) {
		if(modifiers.isEmpty())
			return "[]";
		StringBuilder sb = new StringBuilder("[");
		for(DataModifier modifier : modifiers) {
			sb.append(modifier.toLoreLine())
					.append(color)
					.append("; ");
		}
		return sb.append("]").toString();
	}
	
	public static class ItemDataBDD implements Serializer<ItemData> {
		
		@SerializedName("identifier") public String id;
		@SerializedName("material") public String type;
		public String rarity;
		public String name;
		public boolean magical;
		public boolean archived;
		public boolean droppable;
		@SerializedName("itemstat_set") public DataModifier.DataModifierSerializer[] modifiers;
		public String lore;
		public McEnchantment[] enchants;
		
		public ItemDataBDD() {}
		public ItemDataBDD(ItemData data) {
			this.id = data.id;
			this.name = data.name;
			this.type = data.type.name();
			this.magical = data.magical;
			this.droppable = data.droppable;
			this.rarity = data.rarity.name();
			this.lore = SerializationUtils.loreToString(data.lore);
			this.modifiers = (DataModifier.DataModifierSerializer[]) data.modifiers.stream().map(DataModifier::serialize).toArray();
			this.enchants = data.enchants.toArray(new McEnchantment[0]);
		}
		
		@Override
		public ItemData deserialize() {
			Rarity rarity = Rarity.deserialize(this.rarity);
			if(rarity == null) {
				Shagolur.error("Item [id="+id+"] as a bad rarity : \""+this.rarity+"\"");
				rarity = Rarity.COMMON;
			}
			Material type;
			try {
				type = Material.valueOf(this.type);
			} catch(IllegalArgumentException ignored) {
				Shagolur.error("Item [id="+id+"] as a bad material type : \""+this.type+"\"");
				type = Material.BARRIER;
			}
			List<String> lore = SerializationUtils.stringToLore(this.lore);
			Set<DataModifier> modifiers = Stream.of(this.modifiers).map(bbdDm -> bbdDm.toModifier(id)).collect(Collectors.toSet());
			//Set<McEnchantment> enchants = Set.of(this.enchants);
			//TODO and enchants
			return new ItemData(id, type, magical, droppable, rarity, name, modifiers, lore, Collections.emptySet());
		}
	}
}
