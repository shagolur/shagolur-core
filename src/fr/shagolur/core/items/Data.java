package fr.shagolur.core.items;

import java.util.Arrays;

public enum Data {
	
	INVALID("NULL", "Invalid_data"),
	
	PHYSICAL_DMG("dp", "dégâts physiques"),
	MAGIC_DMG("dm", "dégâts masiques"),
	
	PHYSICAL_RES("rp", "résistance physique"), //◈
	MAGIC_RES("rm", "résistance magique"), // ๑
	
	BONUS_HEALTH("bhe", "max vie §c(❤)"),
	BONUS_MANA("bma", "max mana §b(✦)"),//✤✥✦❉
	MOVEMENT_SPEED("bsm", "de vitesse"),
	BONUS_ATTACK_SPEED("bsa", "de vitesse d'attaque"),
	BONUS_KB_RESIS("bkr", "de résistance au recul"),
	LUCK("luk", "de chance"),
	;
	
	public final String key, description;
	Data(String key, String description) {
		this.key = key;
		this.description = description;
	}
	public String serialize() {
		return name();
	}
	public static Data deserialize(String value) {
		return Arrays.stream(values()).filter(m -> m.name().equals(value)).findFirst().orElse(null);
	}
	
}
