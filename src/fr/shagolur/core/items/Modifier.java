package fr.shagolur.core.items;

import java.util.Arrays;

public enum Modifier {
	
	ADD_FLAT("ADDITIVE"),
	MUL_FLAT("MULTI_FLAT"),
	;
	
	private final String val;
	Modifier(String val) {
		this.val = val;
	}
	
	public String serialize() {
		return val;
	}
	public static Modifier deserialize(String value) {
		return Arrays.stream(values()).filter(m -> m.val.equals(value)).findFirst().orElse(null);
	}
	
}
