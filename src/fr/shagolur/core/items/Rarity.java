package fr.shagolur.core.items;

import org.bukkit.ChatColor;

import java.util.stream.Stream;

public enum Rarity {
	
	DEV(ChatColor.DARK_GREEN, true),
	TEST(ChatColor.DARK_AQUA),
	HONOR(ChatColor.RED),
	
	COMMON(ChatColor.GRAY),
	UNCOMMON(ChatColor.GREEN),
	RARE(ChatColor.BLUE),
	EPIC(ChatColor.DARK_PURPLE),
	UNIQUE(ChatColor.DARK_RED, true),
	LEGENDARY(ChatColor.GOLD, true),
	MYTHICAL(ChatColor.AQUA, true),
	UNIVERSAL(ChatColor.WHITE, true);
	
	public final String color;
	Rarity(ChatColor color) {
		this(color, false);
	}
	Rarity(ChatColor color, boolean bold) {
		this.color = color + "" + (bold?ChatColor.BOLD:"");
	}
	
	public static Rarity deserialize(String val) {
		return Stream.of(values()).filter(r -> r.name().equals(val)).findFirst().orElse(null);
	}
	public String getDisplayName() {
		return color + name();
	}
}
