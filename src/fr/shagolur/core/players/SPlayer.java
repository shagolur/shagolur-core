package fr.shagolur.core.players;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.entities.SEntityType;
import fr.shagolur.core.events.ShagolurPlayerDeathEvent;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.core.storage.Storable;
import fr.shagolur.core.storage.common.BufferInventory;
import fr.shagolur.core.storage.common.SerializerSPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.EquipmentSlot;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import java.util.UUID;

public class SPlayer extends SEntity implements Storable {
	
	private boolean alreadyInBDD;
	private boolean connected;
	
	private Player playerEntity;
	private final UUID uuid;
	private long exp;
	private int level;
	private int money;
	private final BufferInventory bi;
	private Location respawnLoc;
	
	private double mana, manaRegen;
	private String nickname;
	
	public SPlayer(UUID uuid, @Nullable String nickname, long exp, double bufferHealth, double bufferMana, boolean isInBDD, @NotNull BufferInventory bi, Location loc, int money) {
		super(SEntityType.PLAYER);
		this.uuid = uuid;
		this.nickname = nickname;
		this.exp = exp;
		alreadyInBDD = isInBDD;
		this.bi = bi;
		// Transcient data
		displayName = (nickname == null ? uuid.toString() : nickname) + " §7(NC)";
		connected = false;
		// Loots
		((PlayerLoots)super.lootable).link(this);
		// Stats
		super.stats = new PlayerStats(this);
		this.money = money;
		health = bufferHealth == 0 ? getMaxHealth() : bufferHealth;
		mana = bufferMana == 0 ? getMaxMana() : bufferMana;
		healthPerSecond = 0.5; //TODO vraie valeur
		manaRegen = 1; //TODO vraie valeur
		
		//TODO respawn
	}
	
	@Override
	public UUID getUUID() {
		return uuid;
	}
	
	public Player getPlayer() {
		return playerEntity;
	}
	public Player getEntity() {
		return playerEntity;
	}
	
	public PlayerStats getStats() {
		return (PlayerStats) stats;
	}
	
	public void updatePartEquipment(EquipmentSlot slot, ItemData data) {
		getEquipment().updatePart(slot, data);
		getStats().updateEquipment();
	}
	
	public int getMoney() {
		return money;
	}
	public boolean hasMoney(int amount) {
		return money >= amount;
	}
	public void addMoney(int amount) {
		money += money;
	}
	public void consumeMoney(int amount) {
		money -= amount;
	}
	
	public void connect(@NotNull Player playerEntity) {
		if(isConnected())
			return;
		if(!uuid.equals(playerEntity.getUniqueId()))
			throw new RuntimeException("Player " + playerEntity.getName() + " as uuid " + playerEntity.getUniqueId()
				+ "' which is NOT compatible with SPlayer uuid : '"+uuid+"'.");
		this.playerEntity = playerEntity;
		connected = true;
		if(nickname == null)
			nickname = playerEntity.getName();
		displayName = nickname;
		// Lors buffer equipement
		bi.exportToInventory(playerEntity.getInventory());
		bi.clear();
		// Equipement
		recalculateEquipment();
		// Actions
		getStats().updateBasics();
		getStats().updateEquipment();
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public boolean recalculateLevel() {
		int old = level;
		level = Levels.getLevelOf(exp);
		getStats().updateBasics();
		return old != level;
	}
	
	public void recalculateEquipment() {
		if(!isConnected())
			return;
		getEquipment().updateData();
		getStats().updateEquipment();
	}
	
	public void sendMessage(String message) {
		if(isConnected())
			getPlayer().sendMessage(Shagolur.prefix() + message);
	}
	public void sendError(String message) {
		sendMessage(ChatColor.RED + message);
	}
	public void sendSuccess(String message) {
		sendMessage(ChatColor.GREEN + message);
	}
	public void sendActionBar(String message) {
		if(isConnected())
			getPlayer().sendActionBar(message);
	}
	
	public boolean isConnected() {
		return connected;
	}
	
	public boolean addExp(long amount) {
		exp += amount;
		sendSuccess("+ "+ChatColor.YELLOW+amount+ChatColor.GREEN+" exp !");
		return recalculateLevel();
	}
	public long getExp() {
		return exp;
	}
	
	public int getLevel() {
		return Math.max(0, level);
	}
	
	protected void afterHealthChanged() {
		if(!isDead() && exists())
			playerEntity.setHealth(20.0 * getHealthRatio());
		updateActionBar();
	}
	
	@Override
	public void die() {
		if(isDead())
			return;
		dead = true;
		playerEntity.setHealth(0);
		Bukkit.getPluginManager().callEvent(new ShagolurPlayerDeathEvent(this));
	}
	
	public BufferInventory getBufferInventory() {
		return bi;
	}
	
	public void respawn() {
		dead = false;
		health = getMaxHealth();
		if(playerEntity != null)
			playerEntity.spigot().respawn();
		//TODO drop du stuff ??
	}
	
	public boolean hasMana(double amount) {
		return mana >= amount;
	}
	public void fillMana() {
		mana = getMaxMana();
	}
	public void consumeMana(double amount) {
		this.mana -= amount;
		updateActionBar();
	}
	public double getManaRegen() {
		return manaRegen;
	}
	public double getMana() {
		return mana;
	}
	public double getMaxMana() {
		return stats.getMaxMana();
	}
	public double getManaRatio() {
		if(mana <= 0 || getMaxMana() <= 0)
			return 0;
		return mana / getMaxMana();
	}
	
	public void disconnect() {
		if(!isConnected())
			return;
		getBufferInventory().importFromInventory(playerEntity.getInventory());
		this.playerEntity = null;
		connected = false;
		displayName = nickname + " §7(NC)";
	}
	
	@Override
	public Object storageID() {
		return uuid;
	}
	
	@Override
	public Serializer<SPlayer> serialize() {
		if(isConnected())
			getBufferInventory().importFromInventory(playerEntity.getInventory());
		return new SerializerSPlayer(this);
	}
	
	@Override
	public boolean alreadyInBDD() {
		return alreadyInBDD;
	}
	
	@Override
	public void declarePersistentInBDD() {
		alreadyInBDD = true;
	}
	
	public void setMana(double val) {
		this.mana = Math.max(0, Math.min(getMaxMana(), val));
	}
	
	public void setExperience(long val) {
		this.exp = val;
		recalculateLevel();
	}
	
	@Override
	public void tick(double elapsed) {
		super.tick(elapsed);
		if(mana < getMaxMana())
			mana = Math.min(getMaxMana(), mana + getManaRegen() * elapsed);
		updateActionBar();
	}
	
	public void updateActionBar() {
		String healthString = ChatColor.RED + "" + (int)health + "/" + (int)getMaxHealth() + " ❤";
		String manaString = ChatColor.AQUA + "" + (int)mana + "/" + (int)getMaxMana() + " ✦";
		sendActionBar(healthString + "                   " + manaString);
	}
	
}
