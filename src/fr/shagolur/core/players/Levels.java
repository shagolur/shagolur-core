package fr.shagolur.core.players;

import fr.shagolur.core.Shagolur;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public final class Levels {
	private Levels() {}
	
	private static final List<Long> levels = new ArrayList<>(101);
	
	public static void init() throws IOException {
		File file = new File(Shagolur.plugin().getDataFolder(), "levels.txt");
		if(!file.exists()) {
			file.createNewFile();
			InputStream input = Shagolur.plugin().getResource("levels.txt");
			if(input == null)
				throw new IOException("Could not read 'levels.txt' from jar.");
			try(FileOutputStream out = new FileOutputStream(file)) {
				out.write(input.readAllBytes());
			}
			Shagolur.log("Created the levels.txt file.");
		}
		// Read the file
		levels.clear();
		levels.add(0L);
		try(BufferedReader reader = new BufferedReader(new FileReader(file))) {
			String line;
			while((line = reader.readLine()) != null){
				try {
					long exp = Long.parseLong(line);
					levels.add(exp);
				} catch(NumberFormatException ignored) {
					Shagolur.error("Incorrect exp format in levels.txt at line "+(levels.size()-1)+" : '"+line+"'.");
				}
			}
		}
	}
	
	public static int getLevelOf(long exp) {
		for(int i = 1; i < levels.size(); i++)
			if(levels.get(i) > exp)
				return i - 1;
		return levels.size();
	}
	
	public static long getExpForLevel(int level) {
		if(level == 0)
			return 0;
		if(level >= levels.size())
			return levels.size();
		return levels.get(level);
	}
	
}
