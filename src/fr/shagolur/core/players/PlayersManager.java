package fr.shagolur.core.players;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.common.Loopable;
import fr.shagolur.core.storage.StorageSystem;
import fr.shagolur.core.storage.common.BufferInventory;
import fr.shagolur.core.storage.common.PlayerPathProvider;
import fr.shagolur.core.utils.Loop;
import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * Class which handle SPlayers.
 * @see SPlayer
 * @see Shagolur
 */
public class PlayersManager implements Loopable {
	
	private final Map<UUID, SPlayer> players = new HashMap<>();
	private final Loop loop;
	
	private final StorageSystem storage;
	public PlayersManager() {
		storage = Shagolur.getStorage();
		// Register the provider
		storage.registerPathProvider(new PlayerPathProvider());
		
		loop = new Loop(this::tick, 20L);
	}
	
	public Stream<SPlayer> streamOnline() {
		return players.values()
				.stream()
				.filter(SPlayer::isConnected);
	}
	
	public static boolean uuidIsPlayer(UUID uuid) {
		Entity en = Bukkit.getEntity(uuid);
		return en instanceof Player;
	}
	
	public @NotNull SPlayer getPlayer(UUID uuid) {
		if(players.containsKey(uuid))
			return players.get(uuid);
		if(!uuidIsPlayer(uuid))
			throw new IllegalArgumentException("The UUID ("+uuid+") does NOT represent a player !");
		// Exists ?
		SPlayer player = storage.getUnique(SPlayer.class, true, uuid);
		if(player == null) {
			player = new SPlayer(uuid, null,0, 20, 20, false, new BufferInventory(), null, 0);
		}
		// Buffer it and return it.
		players.put(uuid, player);
		return player;
	}
	
	public @NotNull SPlayer getPlayer(Player player) {
		return getPlayer(player.getUniqueId());
	}
	
	
	@Override
	public void startLoop() {
		loop.startLoop();
	}
	
	@Override
	public void stopLoop() {
		loop.stopLoop();
	}
	
	private void tick(double elapsed) {
		players.values().forEach(p -> p.tick(elapsed));
	}
	
	public void save(SPlayer player) {
		storage.saveUnique(SPlayer.class, player);
	}
	
	public void flushData() {
		Shagolur.log("Flushing players data.");
		players.values().forEach(this::save);
		players.entrySet().removeIf(entry -> ! entry.getValue().isConnected());
	}
	
}
