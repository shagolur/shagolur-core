package fr.shagolur.core.players;

import fr.shagolur.core.entities.StatsHolder;
import fr.shagolur.core.items.DataModifier;
import fr.shagolur.core.items.Modifier;

import java.util.Set;

public class PlayerStats implements StatsHolder {
	
	private final SPlayer player;
	
	//TODO refaire ça avec des maps
	private double baseMaxHealth, baseMaxMana, baseRM, baseRP, baseDM, baseDP, baseLuck;
	double flatMaxHealth = 0, flatMaxMana = 0, flatRM = 0, flatRP = 0, flatDM = 0, flatDP = 0, flatLuck = 0;
	double multMaxHealth = 0, multMaxMana = 0, multRM = 0, multRP = 0, multDM = 0, multDP = 0, multLuck = 0;
	
	PlayerStats(SPlayer player) {
		this.player = player;
		updateBasics();
	}
	
	void updateBasics() {
		int level = player.getLevel();
		baseMaxHealth = 100 + (10*level);
		baseMaxMana = 10 + (int)(level/2.);
		baseDM = baseDP = 1 + (int)(level / 5.);     // 1 pt / 5 levels en dégâts
		baseRM = baseRP = (int)(level / 15.);    // 1 pt / 15 levels en résistance
		baseLuck = 1 + (0.01*(int)(level/50.));  // 1% de plus / 50 levels en luck
	}
	
	void updateEquipment() {
		Set<DataModifier> allModifiers = player.getEquipment().getAllModifiers();
		flatMaxHealth = flatMaxMana = flatRM = flatRP = flatDM = flatDP = flatLuck = 0;
		multMaxHealth = multMaxMana = multRM = multRP = multDM = multDP = multLuck = 1;
		for(DataModifier dataModifier : allModifiers) {
			if(dataModifier.modifier() == Modifier.ADD_FLAT) {
				switch (dataModifier.data()) {
					case BONUS_HEALTH -> flatMaxHealth += dataModifier.value();
					case BONUS_MANA -> flatMaxMana += dataModifier.value();
					case PHYSICAL_DMG -> flatDP += dataModifier.value();
					case PHYSICAL_RES -> flatRP += dataModifier.value();
					case MAGIC_DMG -> flatDM += dataModifier.value();
					case MAGIC_RES -> flatRM += dataModifier.value();
					case LUCK -> flatLuck += dataModifier.value();
				}
			} else {
				switch (dataModifier.data()) {
					case BONUS_HEALTH -> multMaxHealth += dataModifier.value();
					case BONUS_MANA -> multMaxMana += dataModifier.value();
					case PHYSICAL_DMG -> multDP += dataModifier.value();
					case PHYSICAL_RES -> multRP += dataModifier.value();
					case MAGIC_DMG -> multDM += dataModifier.value();
					case MAGIC_RES -> multRM += dataModifier.value();
					case LUCK -> multLuck += dataModifier.value();
				}
			}
		}
	}
	
	@Override
	public double getMaxHealth() {
		return (baseMaxHealth + flatMaxHealth) * multMaxHealth;
	}
	
	@Override
	public double getMaxMana() {
		return (baseMaxMana + flatMaxMana) * multMaxMana;
	}
	
	@Override
	public double getPhysicalDamage() {
		return (baseDP + flatDP) * multDP;
	}
	
	@Override
	public double getMagicalDamage() {
		return (baseDM + flatDM) * multDM;
	}
	
	@Override
	public double getPhysicalResistance() {
		return (baseRP + flatRP) * multRP;
	}
	
	@Override
	public double getMagicalResistance() {
		return (baseRM + flatRM) * multRM;
	}
	
	@Override
	public double getLuck() {
		return (baseLuck + flatLuck) * multLuck;
	}
	
	@Override
	public int getLevel() {
		return player.getLevel();
	}
	
	@Override
	public StatsHolder clone() {
		try {
			return (StatsHolder) super.clone();
		} catch(CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public String toString() {
		return "PlayerStats{" +
				"vie=" + getMaxHealth() +
				", mana=" + getMaxMana() +
				", DP=" + getPhysicalDamage() +
				", DM=" + getMagicalDamage() +
				", RP=" + getPhysicalResistance() +
				", RM=" + getMagicalResistance() +
				", LCK=" + getLuck() +
				'}';
	}
}
