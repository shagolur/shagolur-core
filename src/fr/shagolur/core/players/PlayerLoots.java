package fr.shagolur.core.players;

import fr.shagolur.core.entities.Lootable;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.List;

public class PlayerLoots implements Lootable {
	
	private SPlayer linkedPlayer;
	
	public void link(SPlayer player) {
		linkedPlayer = player;
	}
	
	@Override
	public List<ItemStack> loot(double chance) {
		return Collections.emptyList();
	}
}
