package fr.shagolur.core.common;

import java.util.UUID;

public interface Identifiable {
	
	UUID getUUID();
	
}
