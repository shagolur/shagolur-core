package fr.shagolur.core.common;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Describes an object that hold some special metadata.
 */
public interface DataHolder {
	
	/**
	 * Set a metadata
	 * @param key the key to set the metadata with.
	 * @param data the object to store.
	 */
	void setData(@NotNull String key, @NotNull Object data);
	
	/**
	 * Get a metadata
	 * @param key the key of the metadata
	 * @param clazz the clazz to cast the metadata with
	 * @return null if no data corresponds to this key
	 * @throws ClassCastException if a data has this key, but it does not have the right type.
	 */
	<T> @Nullable T getData(@NotNull String key, @NotNull Class<T> clazz) throws ClassCastException;
	
}
