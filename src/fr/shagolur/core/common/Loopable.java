package fr.shagolur.core.common;

public interface Loopable {
	
	void startLoop();
	
	void stopLoop();
	
}
