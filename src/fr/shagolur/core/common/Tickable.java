package fr.shagolur.core.common;

public interface Tickable {
	
	void tick(double elapsed);
	
}
