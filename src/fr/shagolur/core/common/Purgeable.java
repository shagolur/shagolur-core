package fr.shagolur.core.common;

public interface Purgeable {
	
	void purge();
	
}
