package fr.shagolur.core.effects;

import fr.shagolur.core.entities.SEntity;

public record EffectDescriptor(String identifier) {
	
	public Effect instanciate(SEntity target, SEntity source, double duration, double power) {
		return new Effect(this, target, source, duration, power);
	}
	
	void apply(SEntity target, SEntity source, double amount) {
	
	}
	
}
