package fr.shagolur.core.effects;

import fr.shagolur.core.entities.SEntity;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;

public class EffectsManager {
	
	private final Map<String, EffectDescriptor> effectDescriptors = new HashMap<>();
	
	public void register(EffectDescriptor descriptor) {
		effectDescriptors.put(descriptor.identifier(), descriptor);
	}
	
	public boolean exists(String effectID) {
		return effectDescriptors.containsKey(effectID);
	}
	
	public @Nullable Effect create(String effectID, SEntity target, SEntity source, double duration, double multiplicator) {
		EffectDescriptor descriptor = effectDescriptors.get(effectID);
		if(descriptor == null)
			return null;
		return descriptor.instanciate(target, source, duration, multiplicator);
	}
	
}
