package fr.shagolur.core.effects;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.common.Tickable;

public class Effect implements Tickable {
	
	private final long tickEnds;
	private final EffectDescriptor descriptor;
	
	private final SEntity target, source;
	private final double power;
	
	public Effect(EffectDescriptor descriptor, SEntity target, SEntity source, double duration, double power) {
		this.descriptor = descriptor;
		tickEnds = System.currentTimeMillis() + (long)(duration * 1000.0);
		this.target = target;
		this.source = source;
		this.power = power;
	}
	
	public boolean isOver() {
		return System.currentTimeMillis() > tickEnds;
	}
	
	public void tick(double elapsed) {
		descriptor.apply(target, source, power*elapsed);
	}
	
	public double getPower() {
		return power;
	}
	
	public boolean isSelfMitigated() {
		return target.getUUID().equals(source.getUUID());
	}
	
	public String getIdentifier() {
		return descriptor.identifier();
	}
	
}
