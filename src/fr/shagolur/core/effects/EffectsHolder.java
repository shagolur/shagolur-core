package fr.shagolur.core.effects;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.common.Tickable;

public interface EffectsHolder extends Tickable {
	
	boolean hasEffect(String identifier);
	
	void addEffect(String identifier, SEntity source, double duration, double force);
	void addEffect(Effect effect);
	
}
