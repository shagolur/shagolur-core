package fr.shagolur.core.effects;

import fr.shagolur.core.entities.SEntity;

public class PermanentEffect extends Effect {
	
	public PermanentEffect(EffectDescriptor descriptor, SEntity target) {
		super(descriptor, target, target, -1, 1);
	}
	
	public boolean isOver() {
		return false;
	}
}
