package fr.shagolur.core.commands;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.MonsterData;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.ui.MonstersListGUI;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class ShagolurMobsCommand extends JamCommand {
	
	public ShagolurMobsCommand(String command) {
		super(command);
		addArgument(0, "spawn", "ui", "reload", "purge");
		addArgumentIf(
				1,
				new JamArg[] {new JamArg(0, "spawn")},
				() -> Shagolur.getEntitiesManager()
						.getStream()
						.map(MonsterData::getId)
						.toList()
		);
		//TODO argument pour purger uniquement sur certains worlds ?
	}
	
	@Override
	public void onCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
		if(args.length == 0 || args[0].equals("ui")) {
			if(!(sender instanceof Player)) {
				reject(sender, false, label);
				return;
			}
			new MonstersListGUI((Player)sender);
			return;
		}
		if(args[0].equals("reload")) {
			Shagolur.getEntitiesManager().reloadFromBase();
			sender.sendMessage(Shagolur.prefix() + ChatColor.GREEN + "Monsters data from database reloaded.");
			return;
		}
		if(args[0].equals("purge")) {
			Shagolur.getEntitiesManager().purge();
			sender.sendMessage(Shagolur.prefix() + ChatColor.GREEN + "Monster have been removed from worlds.");
			return;
		}
		if(args[0].equals("spawn")) {
			if(!(sender instanceof Player)) {
				reject(sender, false, label);
				return;
			}
			SPlayer player = Shagolur.getPlayersManager().getPlayer((Player)sender);
			if(args.length < 2) {
				player.sendError("Il faut spécifier l'id du mob à spawner !");
				return;
			}
			String id = args[1];
			SEntity entity = Shagolur.getEntitiesManager().spawn(id, player.getLocation());
			if(entity == null) {
				player.sendError("L'id \""+id+"\" ne correspond à aucun monstre.");
				return;
			}
			player.sendSuccess("Monstre spawné.");
			return;
		}
		sender.sendMessage(Shagolur.prefix() + ChatColor.RED + "Sous-commande inconnue. Valeurs acceptées : [ui, purge, spawn, reload].");
	}
	
	@Override
	public CommandExecutors getAllowedExecutors() {
		return CommandExecutors.ALL;
	}
}
