package fr.shagolur.core.commands;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.MonsterData;
import fr.shagolur.core.items.Data;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.players.Levels;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.utils.SerializationUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Supplier;

public class ShagolurDebugCommand extends JamCommand {
	
	private final Map<String, BiConsumer<String,CommandSender>> additionalBehaviours = new HashMap<>();
	
	private String listAvailables() {
		List<String> all = new ArrayList<>();
		all.addAll(List.of("player", "item", "mob"));
		all.addAll(additionalBehaviours.keySet());
		return Arrays.toString(all.toArray());
	}
	
	public ShagolurDebugCommand(String command) {
		super(command);
		
		addArgument(0, "data", "set");
		addArgumentIf(
				1,
				new JamArg[] {new JamArg(0, "data")},
				"player", "mob", "item"
		);
		addArgumentIf(
				1,
				new JamArg[] {new JamArg(0, "set")},
				"mana", "health", "exp", "level"
		);
		addArgumentIf(
				2,
				new JamArg[] {new JamArg(0, "data"), new JamArg(1, "player")},
				() -> Shagolur.getPlayersManager()
						.streamOnline()
						.map(SPlayer::getNickname)
						.toList()
		);
		addArgumentIf(
				2,
				new JamArg[] {new JamArg(0, "data"), new JamArg(1, "item")},
				() -> Shagolur.getItemsManager()
						.getStream()
						.map(ItemData::getId)
						.toList()
		);
		addArgumentIf(
				2,
				new JamArg[] {new JamArg(0, "data"), new JamArg(1, "mob")},
				() -> Shagolur.getEntitiesManager()
						.getStream()
						.map(MonsterData::getId)
						.toList()
		);
		addArgumentIf(
				3,
				new JamArg[] {new JamArg(0, "set"), new JamArg(1, "mana", "health", "exp", "level")},
				() -> Shagolur.getPlayersManager()
						.streamOnline()
						.map(SPlayer::getNickname)
						.toList()
		);
	}
	
	public void addDataExtension(String type, Supplier<Collection<String>> argumentSupplier, BiConsumer<String, CommandSender> behaviour) {
		// add auto-completion
		addArgumentIf(
				2,
				new JamArg[] {new JamArg(0, "data"), new JamArg(1, type)},
				argumentSupplier
		);
		// Register behaviour
		additionalBehaviours.put(type, behaviour);
	}
	
	@Override
	public void onCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
		//SPlayer player = Shagolur.getPlayersManager().getPlayer((Player)sender);
		if(args.length < 3) {
			sendError(sender, "Pas assez d'arguments");
			return;
		}
		if(args[0].equals("data")) {
			if(args[1].equals("player")) {
				SPlayer target = Shagolur.getPlayersManager()
						.streamOnline()
						.filter(n -> n.getNickname().equals(args[2]))
						.findFirst().orElse(null);
				if(target == null) {
					sendError(sender, "Joueur inconnu : \""+args[2]+"\".");
					return;
				}
				sendSuccess(sender, "------- [PLAYER DATA {"+target.getName()+"§a}] -------");
				sendInfo(sender, "Online ? " + (target.isConnected()?"§atrue":"§cfalse") + "§e, exists ? "+ (target.exists()?"§atrue":"§cfalse")+"§e.");
				sendInfo(sender, "Lvl: " + target.getLevel() + ", exp="+target.getExp());
				sendInfo(sender, "Money : §6" + target.getMoney());
				sendInfo(sender, "HP: §a" + target.getHealth()+"/"+target.getMaxHealth() + "§e, mana=§b"+target.getMana()+"/"+target.getMaxMana());
				sendInfo(sender, "Position : " + SerializationUtils.niceLocation(target.getLocation(), "§e"));
				sendInfo(sender, "Equipement : " + target.getEquipment().toString("§e"));
				sendInfo(sender, "Stats : " + target.getStats().toString());
				sendInfo(sender, "------------------------------------------");
				return;
			}
			if(args[1].equals("item")) {
				ItemData item = Shagolur.getItemsManager().get(args[2]);
				if(item == null) {
					sendError(sender, "Item inconnu : \""+args[2]+"\".");
					return;
				}
				sendSuccess(sender, "------- [ITEM DATA {"+item.getDisplayName()+"§a}] -------");
				sendInfo(sender, "BddID : §b" + item.getId() + "§e, rarity : " + item.getRarity().getDisplayName());
				sendInfo(sender, "Material : §f" + item.getType().name() + "§e, type : " + (item.isMagical()?"§bmagic":"§3physic"));
				sendInfo(sender, "Lore : " + (item.getLore().isEmpty() ? "§7none":""));
				item.getLore().forEach(l -> sendInfo(sender, " - " + l));
				sendInfo(sender, "Stats : " + (item.getModifiers().isEmpty() ? "§7none":""));
				item.getModifiers().forEach(m -> sendInfo(sender, " - " + m.toLoreLine()));
				sendInfo(sender, "Enchants : " + (item.getEnchants().isEmpty() ? "§7none":""));
				item.getEnchants().forEach(e -> sendInfo(sender, " - " + e.toLoreLine()));
				sendInfo(sender, "------------------------------------------");
				return;
			}
			if(args[1].equals("mob")) {
				MonsterData monster = Shagolur.getEntitiesManager().get(args[2]);
				if(monster == null) {
					sendError(sender, "Mob inconnu : \""+args[2]+"\".");
					return;
				}
				sendSuccess(sender, "------- [MOB DATA {"+monster.getDisplayName()+"§a}] -------");
				sendInfo(sender, "BddID : §b" + monster.getId());
				sendInfo(sender, "EntityType : §f" + monster.getEntityType().name() + "§e, exp : §a" + monster.getExpDrop());
				sendInfo(sender, "Stats : ");
				for(Data d : Data.values()) {
					double v = monster.getStats().getData(d);
					if(v != 0)
						sendInfo(sender, "- " + d + " : " + v);
				}
				sendInfo(sender, "Equipment : " + (monster.getEquipment().isEmpty()?"§7none":""));
				monster.getEquipment().forEach((key, value) -> sendInfo(sender, " - " + key + " : " + value.getName()));
				sendInfo(sender, "Loots : " + (monster.getLoots().isEmpty()?"§7none":""));
				monster.getLoots().getSerializableEntries().forEach(e -> sendInfo(sender, " - §b" + e.getKey() + "§e : §f" + e.getValue()));
				sendInfo(sender, "isBaby : "+(monster.isBabie()?"§atrue":"§cfalse")+"§e, isInvisible ? " + (monster.isInvisible()?"§atrue":"§cfalse"));
				sendInfo(sender, "------------------------------------------");
				return;
			}
			if(additionalBehaviours.containsKey(args[1])) {
				additionalBehaviours.get(args[1]).accept(args[2], sender);
				return;
			}
			sendError(sender, "Sous-commande inconnue. Valeurs acceptées : "+listAvailables());
			return;
		}
		if(args[0].equals("set")) {
			double val;
			try {
				val = Double.parseDouble(args[2]);
			} catch(NumberFormatException e) {
				sendError(sender,"Bad number format : \""+args[2]+"\".");
				return;
			}
			SPlayer target;
			if(args.length == 3) {
				if(sender instanceof Player) {
					target = Shagolur.getPlayersManager().getPlayer((Player) sender);
				} else {
					reject(sender, false, "/"+label+" "+args[0]);
					return;
				}
			} else {
				target = Shagolur.getPlayersManager()
						.streamOnline()
						.filter(n -> n.getNickname().equals(args[3]))
						.findFirst().orElse(null);
				if(target == null) {
					sendError(sender, "Could not find target player \""+args[3]+"\".");
					return;
				}
			}
			switch (args[1]) {
				case "mana" -> {
					target.setMana(val);
					sendSuccess(sender, "Le mana de " + target.getName() + "§a a été forcé à " + val + ".");
					if (target != sender)
						target.sendSuccess("Un administrateur a forcé votre mana à " + val + ".");
				}
				case "health" -> {
					target.setHealth(val);
					sendSuccess(sender, "La vie de " + target.getName() + "§a a été forcée à " + val + ".");
					if (target != sender)
						target.sendSuccess("Un administrateur a forcé vos points de vie à " + val + ".");
				}
				case "exp" -> {
					target.setExperience((long) val);
					sendSuccess(sender, "Exp de " + target.getName() + "§a a été forcé à " + val + ". Nouveau niveau : " + target.getLevel() + ".");
					if (target != sender)
						target.sendSuccess("Un administrateur a forcé vos points d'expériences à " + val + "." + ". Nouveau niveau : " + target.getLevel() + ".");
				}
				case "level" -> {
					target.setExperience(Levels.getExpForLevel((int) val));
					sendSuccess(sender, "Le niveau de de " + target.getName() + "§a a été forcé à " + target.getLevel() + ".");
					if (target != sender)
						target.sendSuccess("Un administrateur a forcé votre niveau à " + target.getLevel() + ".");
				}
				default -> sendError(sender, "Unknown sub-command \"" + args[1] + "\".");
			}
			return;
		}
		sendError(sender, "Sous-commande inconnue. Valeurs acceptées : [data,set].");
	}
	
	@Override
	public CommandExecutors getAllowedExecutors() {
		return CommandExecutors.ALL;
	}
}
