package fr.shagolur.core.commands;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.ui.ItemsListGUI;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

public class ShagolurItemCommand extends JamCommand {
	
	public ShagolurItemCommand(String command) {
		super(command);
		addArgument(0, "give", "ui", "reload");
		addArgumentIf(
				1,
				new JamArg[] {new JamArg(0, "give")},
				() -> Shagolur.getItemsManager()
						.getStream()
						.map(ItemData::getId)
						.toList()
		);
	}
	
	@Override
	public void onCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
		//SPlayer player = Shagolur.getPlayersManager().getPlayer((Player)sender);
		if(args.length == 0 || args[0].equals("ui")) {
			if(!(sender instanceof Player)) {
				reject(sender, false, label);
				return;
			}
			new ItemsListGUI((Player)sender);
			return;
		}
		if(args[0].equals("reload")) {
			Shagolur.getItemsManager().reloadFromBase();
			sender.sendMessage(Shagolur.prefix() + ChatColor.GREEN + "Items from database reloaded.");
			return;
		}
		if(args[0].equals("give")) {
			if(!(sender instanceof Player)) {
				reject(sender, false, label);
				return;
			}
			SPlayer player = Shagolur.getPlayersManager().getPlayer((Player)sender);
			if(args.length < 2) {
				player.sendError("Il faut spécifier l'item à donner !");
				return;
			}
			String id = args[1];
			ItemData item = Shagolur.getItemsManager().get(id);
			if(item == null) {
				player.sendError("L'id \""+id+"\" ne correspond à aucun item.");
				return;
			}
			player.getPlayer().getInventory().addItem(item.toItemStack());
			player.sendSuccess("Item bien reçu.");
			return;
		}
		sender.sendMessage(Shagolur.prefix() + ChatColor.RED + "Sous-commande inconnue. Valeurs acceptées : [ui, give, reload].");
	}
	
	@Override
	public CommandExecutors getAllowedExecutors() {
		return CommandExecutors.ALL;
	}
}
