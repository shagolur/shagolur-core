package fr.shagolur.core.commands;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.events.ShagolurStorageReloadEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.jetbrains.annotations.NotNull;

public class ShagolurStorageCommand extends JamCommand {
	
	public ShagolurStorageCommand(String command) {
		super(command);
		addArgument(0, "reload");
	}
	
	@Override
	public void onCommand(@NotNull CommandSender sender, @NotNull String label, @NotNull String[] args) {
		if(args[0].equals("reload")) {
			Shagolur.getItemsManager().reloadFromBase();
			Shagolur.getEntitiesManager().reloadFromBase();
			Shagolur.getPlayersManager().flushData();
			Bukkit.getPluginManager().callEvent(new ShagolurStorageReloadEvent(sender));
			sender.sendMessage(Shagolur.prefix() + ChatColor.GREEN + "All elements from database were reloaded.");
			return;
		}
		sender.sendMessage(Shagolur.prefix() + ChatColor.RED + "Sous-commande inconnue. Valeurs acceptées : [reload].");
	}
	
	@Override
	public CommandExecutors getAllowedExecutors() {
		return CommandExecutors.ALL;
	}
}
