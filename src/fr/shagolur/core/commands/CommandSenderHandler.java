package fr.shagolur.core.commands;

import fr.shagolur.core.Shagolur;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public interface CommandSenderHandler {
	
	default void sendError(CommandSender sender, String messsage) {
		sender.sendMessage(Shagolur.prefix() + ChatColor.RED + messsage);
	}
	default void sendSuccess(CommandSender sender, String messsage) {
		sender.sendMessage(Shagolur.prefix() + ChatColor.GREEN + messsage);
	}
	default void sendInfo(CommandSender sender, String messsage) {
		sender.sendMessage(Shagolur.prefix() + ChatColor.YELLOW + messsage);
	}

}
