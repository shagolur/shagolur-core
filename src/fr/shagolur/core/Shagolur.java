package fr.shagolur.core;

import fr.shagolur.core.commands.ShagolurDebugCommand;
import fr.shagolur.core.commands.ShagolurItemCommand;
import fr.shagolur.core.commands.ShagolurMobsCommand;
import fr.shagolur.core.commands.ShagolurStorageCommand;
import fr.shagolur.core.effects.EffectsManager;
import fr.shagolur.core.entities.EntitiesManager;
import fr.shagolur.core.listeners.BukkitAggroListener;
import fr.shagolur.core.listeners.BukkitArmorEquipListener;
import fr.shagolur.core.listeners.BukkitBlocksListeners;
import fr.shagolur.core.listeners.BukkitDamageListeners;
import fr.shagolur.core.listeners.BukkitDeathListeners;
import fr.shagolur.core.listeners.BukkitNaturalListeners;
import fr.shagolur.core.listeners.GuiListeners;
import fr.shagolur.core.listeners.JoinLeaveListeners;
import fr.shagolur.core.items.ItemsManager;
import fr.shagolur.core.listeners.PlayerInteractListener;
import fr.shagolur.core.parties.PartiesManager;
import fr.shagolur.core.players.Levels;
import fr.shagolur.core.players.PlayersManager;
import fr.shagolur.core.storage.StorageFactory;
import fr.shagolur.core.storage.StorageSystem;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public final class Shagolur extends JavaPlugin {
	
	/**
	 * Get the plugin instance, in case a method needs a JavaPlugin instance
	 * @return the core plugin instance
	 * @see JavaPlugin
	 */
	public static Shagolur plugin() {
		return PLUGIN;
	}
	private static Shagolur PLUGIN;
	
	/**
	 * Get the plugin prefix, as defined in the configuration
	 * @return a String, can contain colors.
	 */
	public static String prefix() {
		return PLUGIN.prefix;
	}
	
	
	/**
	 * Log a message in console
	 * @param message the message to print
	 */
	public static void log(String message) {
		Bukkit.getConsoleSender().sendMessage(ChatColor.BLUE+"[Shagolur]"+ChatColor.GREEN + "[INFO] " + ChatColor.GRAY + message);
	}
	/**
	 * Log a message in console, as a warning
	 * @param message the message to print
	 */
	public static void warning(String message) {
		Bukkit.getConsoleSender().sendMessage(ChatColor.BLUE+"[Shagolur]"+ChatColor.GOLD + "[WARN] " + ChatColor.YELLOW + message);
	}
	/**
	 * Log a message in console, as an error
	 * @param message the message to print
	 */
	public static void error(String message) {
		Bukkit.getConsoleSender().sendMessage(ChatColor.BLUE+"[Shagolur]"+ChatColor.DARK_RED + "[ERROR] " + ChatColor.RED + message);
	}
	public static void debug(String message) {
		Bukkit.getConsoleSender().sendMessage(ChatColor.BLUE+"[Shagolur]"+ChatColor.DARK_AQUA + "[DEBUG] " + ChatColor.BLUE + message);
	}
	
	private String prefix = "";
	private PlayersManager players;
	private ItemsManager items;
	private EntitiesManager entities;
	private EffectsManager effects;
	private PartiesManager parties;
	private StorageSystem storage;
	// Special commands
	private ShagolurDebugCommand debugCommand;
	
	@Override
	public void onEnable() {
		log("Initialization of Shagolùr-CORE...");
		PLUGIN = this;
		// Load the prefix and the path
		loadPrefix();
		saveDefaultConfig();
		try {
			Levels.init();
		} catch(IOException e) {
			error("Could not init levels exp : " + e.getMessage());
		}
		// Load storage
		storage = StorageFactory.factory(getConfig().getConfigurationSection("storage"));
		if(storage == null) {
			error("Could not create a storage system with the current configuration. Stopping the plugin.");
			getPluginLoader().disablePlugin(this);
			return;
		}
		// Load elements
		items = new ItemsManager();
		players = new PlayersManager();
		effects = new EffectsManager();
		entities = new EntitiesManager();
		parties = new PartiesManager();
		// Load commands
		new ShagolurItemCommand("sg.items");
		new ShagolurStorageCommand("sg.storage");
		new ShagolurMobsCommand("sg.mobs");
		debugCommand = new ShagolurDebugCommand("sg.debug");
		// Load listeners
		new GuiListeners();
		new JoinLeaveListeners();
		new PlayerInteractListener();
		new BukkitDamageListeners();
		new BukkitArmorEquipListener();
		new BukkitDeathListeners();
		new BukkitNaturalListeners();
		new BukkitBlocksListeners();
		new BukkitAggroListener();
		
		// Fake join
		for(Player p : Bukkit.getOnlinePlayers()) {
			Bukkit.getPluginManager().callEvent(new PlayerJoinEvent(p, ""));
		}
		
		log("Shagolùr-CORE has started sucessfuly.");
		
		// Start loops
		entities.startLoop();
		players.startLoop();
	}
	
	@Override
	public void onDisable() {
		log("Stopping Shagolùr-CORE...");
		// Remove entities
		entities.purge();
		// Fake leave
		for(Player p : Bukkit.getOnlinePlayers()) {
			p.closeInventory();
			Bukkit.getPluginManager().callEvent(new PlayerQuitEvent(p, ""));
		}
		// Flush players data
		players.flushData();
		log("Shagolùr-CORE has stopped sucessfuly.");
	}
	
	/**
	 * Get the plugin instance of the Players Manager.
	 * @return an instance of {@link PlayersManager}
	 */
	public static @NotNull PlayersManager getPlayersManager() {
		return PLUGIN.players;
	}
	/**
	 * Get the plugin instance of the Storage System.
	 * @return an instance of {@link StorageSystem}
	 */
	public static @NotNull StorageSystem getStorage() {
		return PLUGIN.storage;
	}
	/**
	 * Get the plugin instance of the Items Manager.
	 * @return an instance of {@link ItemsManager}
	 */
	public static @NotNull ItemsManager getItemsManager() {
		return PLUGIN.items;
	}
	/**
	 * Get the plugin instance of the Entities Manager.
	 * @return an instance of {@link EntitiesManager}
	 */
	public static @NotNull EntitiesManager getEntitiesManager() {
		return PLUGIN.entities;
	}
	public static @NotNull EffectsManager getEffectsManager() {
		return PLUGIN.effects;
	}
	public static @NotNull PartiesManager getPartiesManager() {
		return PLUGIN.parties;
	}
	
	public static @NotNull ShagolurDebugCommand getDebugCommand() {
		return PLUGIN.debugCommand;
	}
	
	private static final Map<String, NamespacedKey> KEYS = new HashMap<>();
	/**
	 * Get a common core NamespacedKey for NBT
	 * @param val the name of the key
	 * @return a NamespacedKey unique for this plugin core and parameter value
	 */
	public static NamespacedKey key(String val) {
		if(val == null) val = "none";
		if(KEYS.containsKey(val))
			return KEYS.get(val);
		NamespacedKey key = new NamespacedKey(plugin(), "mco-" + val);
		KEYS.put(val, key);
		return key;
	}
	
	private void loadPrefix() {
		String rawPrefix = getConfig().getString("prefix");
		if(rawPrefix == null) {
			prefix = "";
			warning("No prefix was defined for the plugin.");
		} else {
			prefix = ChatColor.translateAlternateColorCodes('&', rawPrefix);
		}
	}
}
