package fr.shagolur.core.events;

import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.players.SPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class ShagolurCanAggroRequest extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	private final SPlayer owner;
	private final SEntity entity, target;
	private boolean canHurt;
	
	public ShagolurCanAggroRequest(SPlayer owner, SEntity entity, SEntity target) {
		this.owner = owner;
		this.entity = entity;
		this.target = target;
		this.canHurt = true;
	}
	
	public SPlayer getOwnerPlayer() {
		return owner;
	}
	
	public SEntity getEntity() {
		return entity;
	}
	public SEntity getTarget() {
		return target;
	}
	
	public boolean canHurt() {
		return canHurt;
	}
	
	public void setCanHurt(boolean canHurt) {
		this.canHurt = canHurt;
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
