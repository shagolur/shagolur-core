package fr.shagolur.core.events;

import fr.shagolur.core.players.SPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class ShagolurCanHurtRequest extends Event {
	private static final HandlerList handlers = new HandlerList();
	
	private final SPlayer a,b;
	private boolean canHurt;
	
	public ShagolurCanHurtRequest(SPlayer a, SPlayer b) {
		this.a = a;
		this.b = b;
		this.canHurt = true;
	}
	
	public SPlayer getA() {
		return a;
	}
	
	public SPlayer getB() {
		return b;
	}
	
	public boolean canHurt() {
		return canHurt;
	}
	
	public void setCanHurt(boolean canHurt) {
		this.canHurt = canHurt;
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
