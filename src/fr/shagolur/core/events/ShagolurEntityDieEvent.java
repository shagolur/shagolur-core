package fr.shagolur.core.events;

import fr.shagolur.core.entities.SEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ShagolurEntityDieEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final boolean hasKiller;
	private final SEntity victim;
	private final SEntity killer;
	private final List<ItemStack> loots;
	private final Map<SEntity, Double> percentagesExp = new HashMap<>();
	
	public ShagolurEntityDieEvent(@NotNull SEntity victim, SEntity killer) {
		this.victim = victim;
		this.killer = killer;
		hasKiller = killer != null;
		loots = victim.loots().loot(1.0);
		if(killer != null)
			percentagesExp.put(killer, 1.0);
	}
	public ShagolurEntityDieEvent(SEntity victim) {
		this(victim, victim.getLastDamager());
	}
	
	/**
	 * @return The real instance of loots, any modifications will impact the real loots.
	 */
	public List<ItemStack> getLoots() {
		return loots;
	}
	
	public boolean hasKiller() {
		return hasKiller;
	}
	
	public SEntity getKiller() {
		return killer;
	}
	
	public @NotNull SEntity getVictim() {
		return victim;
	}
	
	public @NotNull Map<SEntity, Double> getPercentagesExp() {
		return percentagesExp;
	}
	
	@Override
	public String toString() {
		if(hasKiller())
			return "ShagolurEntityDieEvent{("+killer.getName()+"§r) killed (" + victim.getName() + "§r)}";
		return "ShagolurEntityDieEvent{of (" + victim.getName() + ")}";
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
