package fr.shagolur.core.events;

import org.bukkit.command.CommandSender;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class ShagolurStorageReloadEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final CommandSender commandSender;
	
	public ShagolurStorageReloadEvent(CommandSender commandSender) {
		this.commandSender = commandSender;
	}
	
	public CommandSender getCommandSender() {
		return commandSender;
	}
	
	@Override
	public String toString() {
		return "ShagolurStorageReloadEvent{commandSender=" + commandSender + '}';
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
