package fr.shagolur.core.events;

import fr.shagolur.core.players.SPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class ShagolurPlayerQuitEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final SPlayer player;
	
	public ShagolurPlayerQuitEvent(SPlayer player) {
		this.player = player;
	}
	
	public SPlayer getPlayer() {
		return player;
	}
	
	@Override
	public String toString() {
		return "ShagolurPlayerQuitEvent{player=" + player + '}';
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
