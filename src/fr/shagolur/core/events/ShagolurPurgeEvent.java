package fr.shagolur.core.events;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class ShagolurPurgeEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	public static final String ALL = "__all__";
	
	private final String[] filters;
	
	public ShagolurPurgeEvent(String... filters) {
		if(filters.length == 0)
			this.filters = new String[] {ALL};
		else
			this.filters = filters;
	}
	
	public boolean hasFilter(String filter) {
		for(String ft : filters)
			if(ft.equals(ALL) || ft.equals(filter))
				return true;
		return false;
	}
	
	@Override
	public String toString() {
		return "ShagolurPurgeEvent{filters=" + Arrays.toString(filters) + '}';
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
