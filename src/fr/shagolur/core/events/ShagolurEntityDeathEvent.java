package fr.shagolur.core.events;

import fr.shagolur.core.entities.SEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class ShagolurEntityDeathEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final SEntity victim;
	
	public ShagolurEntityDeathEvent(SEntity victim) {
		this.victim = victim;
	}
	public SEntity getVictim() {
		return victim;
	}
	
	@Override
	public String toString() {
		return "ShagolurEntityDeathEvent{" + victim.getName() + "§r}";
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
