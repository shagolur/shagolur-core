package fr.shagolur.core.events;

import fr.shagolur.core.players.SPlayer;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class ShagolurPlayerDeathEvent extends Event {
	
	private static final HandlerList handlers = new HandlerList();
	
	private final SPlayer victim;
	
	public ShagolurPlayerDeathEvent(SPlayer victim) {
		this.victim = victim;
	}
	public SPlayer getVictim() {
		return victim;
	}
	
	@Override
	public String toString() {
		return "ShagolurPlayerDeathEvent{" + victim.getName() + "§r}";
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
