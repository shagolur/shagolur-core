package fr.shagolur.core.events;

import fr.shagolur.core.entities.SEntity;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.jetbrains.annotations.NotNull;

public class ShagolurDamageEvent extends Event implements Cancellable {
	
	private static final HandlerList handlers = new HandlerList();
	
	public boolean cancelled = false;
	private final boolean hasDamager;
	private final SEntity victim;
	private final SEntity damager;
	private double currentDamages;
	private boolean physical;
	
	public ShagolurDamageEvent(SEntity victim, SEntity damager, boolean physical, double currentDamages) {
		this.victim = victim;
		this.damager = damager;
		hasDamager = damager != null;
		this.physical = physical;
		this.currentDamages = currentDamages;
	}
	public ShagolurDamageEvent(SEntity victim, boolean physical, double currentDamages) {
		this.victim = victim;
		damager = null;
		hasDamager = false;
		this.physical = physical;
		this.currentDamages = currentDamages;
	}
	
	public boolean hasDamager() {
		return hasDamager;
	}
	
	public boolean areDamagesPhysicals() {
		return physical;
	}
	
	public void setDamagesPhysical(boolean physical) {
		this.physical = physical;
	}
	
	public double getCurrentDamages() {
		return currentDamages;
	}
	
	public SEntity getDamager() {
		return damager;
	}
	
	public SEntity getVictim() {
		return victim;
	}
	
	public void setCurrentDamages(double currentDamages) {
		this.currentDamages = currentDamages;
	}
	
	@Override
	public String toString() {
		if(damager != null)
			return "ShagolurDamageEvent{(§e"+damager.getName()+"§r) --("+currentDamages+")-> (" + victim.getName() + "§r)}";
		return "ShagolurDamageEvent{(x) --("+currentDamages+")-> (" + victim.getName() + "§r)}";
	}
	
	public boolean isCancelled() {
		return cancelled;
	}
	
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
	
	@Override
	public @NotNull HandlerList getHandlers() {
		return handlers;
	}
	public static HandlerList getHandlerList() {
		return handlers;
	}
}
