package fr.shagolur.core.utils;

import java.lang.reflect.Array;

public final class Utils {
	private Utils() {}
	
	public static int randInt(int min, int max) {
		return randInt(Math.random(), min, max);
	}
	
	public static int randInt(double proba, int min, int max) {
		return min + (int)(proba * ((max - min) + 1));
	}
	
	@SuppressWarnings("unchecked")
	public static <T> Class<? extends T[]> getArrayClass(Class<T> clazz) {
		return (Class<? extends T[]>) Array.newInstance(clazz, 0).getClass();
	}
	
}
