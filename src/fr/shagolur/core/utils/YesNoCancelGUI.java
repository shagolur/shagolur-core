package fr.shagolur.core.utils;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;

public abstract class YesNoCancelGUI extends MenuGUI {
	
	public enum Response{YES, NO, CANCEL}

	private static final ItemStack ITEM_NO = new ItemBuilder(Material.RED_CONCRETE).setDisplayName(ChatColor.RED+""+ChatColor.BOLD+"Non !").build();
	private static final ItemStack ITEM_CANCEL = new ItemBuilder(Material.WHITE_CONCRETE).setDisplayName(ChatColor.WHITE+""+ChatColor.BOLD+"Annuler").build();
	private static final ItemStack ITEM_YES = new ItemBuilder(Material.GREEN_CONCRETE).setDisplayName(ChatColor.GREEN+""+ChatColor.BOLD+"Oui !").build();
	
	private static final int SLOT_YES = 2;
	private static final int SLOT_CANCEL = 4;
	private static final int SLOT_NO = 6;
	
	public YesNoCancelGUI(String title) {
		super(title, 9, false);
		init();
	}
	
	public void setCancelItem(ItemStack is){
		addOption(is, SLOT_CANCEL);
	}
	
	public void setNoItem(ItemStack is){
		addOption(is, SLOT_NO);
	}
	
	public void setYesItem(ItemStack is){
		addOption(is, SLOT_YES);
	}
	
	private void init(){
		addOption(ITEM_NO, SLOT_NO);
		addOption(ITEM_CANCEL, SLOT_CANCEL);
		addOption(ITEM_YES, SLOT_YES);
		int i = 0;
		while(i < getSize()){
			addOption(MenuGUI.FILLER);
			i++;
		}
	}
	@Override
	public void onClose(InventoryCloseEvent e) {
		removeFromList();
	}
	
	@Override
	public void onClick(InventoryClickEvent e) {
		e.setCancelled(true);
		switch (e.getSlot()) {
			case SLOT_CANCEL -> onFinish(Response.CANCEL);
			case SLOT_YES -> onFinish(Response.YES);
			case SLOT_NO -> onFinish(Response.NO);
		}
	}
	
	public abstract void onFinish(Response response);
}
