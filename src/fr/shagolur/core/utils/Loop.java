package fr.shagolur.core.utils;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.common.Loopable;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitTask;

import java.util.function.Consumer;

public class Loop implements Loopable {
	
	private final Consumer<Double> called;
	private final long frequency;
	private BukkitTask task;
	
	public Loop(Consumer<Double> called, long frequency) {
		this.called = called;
		this.frequency = frequency;
	}
	
	@Override
	public void startLoop() {
		if(task != null)
			throw new IllegalStateException("Cannot start an already-started loop.");
		task = Bukkit.getScheduler()
				.runTaskTimer(Shagolur.plugin(), this::tick, frequency, frequency);
	}
	
	@Override
	public void stopLoop() {
		if(task == null)
			throw new IllegalStateException("Cannot stop an already-stopped loop.");
		task.cancel();
		task = null;
	}
	
	private double lastTick = 0;
	
	private void tick() {
		if(lastTick == 0) {
			lastTick = System.currentTimeMillis();
			return;
		}
		double now = System.currentTimeMillis();
		double elapsed = now - lastTick;
		called.accept(elapsed/1000.0);
		lastTick = now;
	}
	
}
