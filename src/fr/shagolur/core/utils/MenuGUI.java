package fr.shagolur.core.utils;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.players.SPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class MenuGUI {
	
	public static final ItemStack FILLER = new ItemBuilder(Material.GRAY_STAINED_GLASS_PANE).setDisplayName(ChatColor.GRAY+"").build();
	public static final ItemStack FILLER_DARK = new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName(ChatColor.DARK_GRAY+"").build();
	
	public static MenuGUI checkForMenuClick(JavaPlugin main, InventoryClickEvent e, boolean cancelShift){
		if(e == null || e.getClickedInventory() == null)
			return null;
		if(e.getAction() == InventoryAction.COLLECT_TO_CURSOR) {
			for(MenuGUI gui : guis) {
				if(gui.main.getDescription().getName().equals(main.getDescription().getName())){
					if(
						gui.name.equals(e.getWhoClicked().getOpenInventory().getTitle())
					){
						if(gui.acceptsCollectAllFromInv) {
							gui.onClick(e);
							return gui;
						}
						e.setCancelled(true);
						return null;
					}
				}
			}
			return null;
		}
		for(MenuGUI gui : guis) {
			if(gui.main.getDescription().getName().equals(main.getDescription().getName())){
				if(
						gui.inv.getType() == e.getClickedInventory().getType() &&
						gui.name.equals(e.getView().getTitle()) &&
						gui.inv.getViewers().equals(e.getClickedInventory().getViewers())
				){
					gui.onClick(e);
					return gui;
				}
				if(cancelShift && e.getClick().name().contains("SHIFT") &&
						gui.inv.getType() == e.getView().getTopInventory().getType() &&
						gui.name.equals(e.getView().getTitle()) &&
						gui.inv.getViewers().equals(e.getView().getTopInventory().getViewers())
				){
					e.setCancelled(true);
					return null;
				}
			}
		}
		return null;
	}
	
	/**
	 * Should be called on InventoryCloseEvent, is required for the onClose method to work.
	 * @param e The event called.
	 * @param main The plugin owning the menus.
	 * @return The menu recognized, null if not a menugui that was closed.
	 */
	public static MenuGUI checkForMenuClose(JavaPlugin main, InventoryCloseEvent e){
		if(e == null)
			return null;
		for(MenuGUI gui : guis){
			if(
					gui.main.getDescription().getName().equals(main.getDescription().getName()) &&
							gui.inv.getType() == e.getInventory().getType() &&
							gui.name.equals(e.getView().getTitle()) &&
							gui.inv.getViewers().equals(e.getInventory().getViewers())
			){
				gui.onClose(e);
				return gui;
			}
		}
		return null;
	}
	
	public static final List<MenuGUI> guis = new ArrayList<>();
	private final boolean acceptsCollectAllFromInv;
	private String name;
	private final int size;
	public JavaPlugin main = Shagolur.plugin();
	private Inventory inv;
	public SPlayer player;
	
	/**
	 * Create a new MenuGUI.
	 * @param name The title of the menu.
	 * @param size The size of the menu (Valid options are 9, 18, 27, 36, 45, 54)
	 */
	public MenuGUI(String name, int size, boolean acceptsCollectAllFromInv) {
		this.name = name;
		assert size % 9 == 0;
		this.size = size;
		this.acceptsCollectAllFromInv = acceptsCollectAllFromInv;
		this.inv = Bukkit.createInventory(null, size, name);
		guis.add(this);
	}
	
	public void removeFromList() {
		guis.remove(this);
	}
	
	public abstract void onClose(InventoryCloseEvent e);
	
	public abstract void onClick(InventoryClickEvent e);
	
	public Inventory getInventory(){
		return inv;
	}
	
	public void setTitle(String title){
		this.name=title;
		recreateInventory();
	}
	
	public void recreateInventory(){
		this.inv = Bukkit.createInventory(null, size, name);
	}
	
	public MenuGUI addOption(ItemStack is){
		addOption(is, -1);
		return this;
	}
	
	public void fill(boolean clearAll) {
		for(int i = 0; i < size; i++) {
			if(clearAll || inv.getItem(i) == null)
				inv.setItem(i, FILLER);
		}
	}
	
	public MenuGUI addOption(ItemStack is, int position){
		if(position/9>5)
			return this;
		if(position<0)
			inv.addItem(is);
		else
			inv.setItem(position, is);
		return this;
	}
	
	public void show(SPlayer player) {
		this.player = player;
		player.getPlayer().openInventory(inv);
	}
	
	public void show(Player player) {
		this.player = Shagolur.getPlayersManager().getPlayer(player);
		player.openInventory(inv);
	}
	
	public void show(Collection<Player> pls){
		for(Player player : pls)
			show(player);
	}
	
	public int getSize(){
		return size;
	}
	
	public List<Player> ecivtViewers(){
		return evictViewers(null);
	}
	
	public List<Player> evictViewers(String msg){
		List<Player> viewers = new ArrayList<>();
		for(HumanEntity entity : inv.getViewers()){
			entity.closeInventory();
			if(msg != null)
				entity.sendMessage(msg);
			if(entity instanceof Player){
				viewers.add(((Player)entity));
			}
		}
		return viewers;
	}
}
