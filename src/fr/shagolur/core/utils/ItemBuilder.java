package fr.shagolur.core.utils;

import com.destroystokyo.paper.profile.PlayerProfile;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeModifier;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.CompassMeta;
import org.bukkit.inventory.meta.Damageable;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.inventory.meta.SuspiciousStewMeta;
import org.bukkit.persistence.PersistentDataContainer;
import org.bukkit.persistence.PersistentDataType;
import org.bukkit.potion.PotionEffect;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Util class to properly handle items
 * @author jamailun
 */
public class ItemBuilder {
	
	private int amount;
	private Material type;
	private boolean unbreakable;
	private String displayName;
	private final List<String> lore = new ArrayList<>();
	private final Set<ItemFlag> flags = new HashSet<>();
	private final Map<Enchantment, Integer> enchants = new HashMap<>();
	private final Map<Attribute, Collection<AttributeModifier>> modifiers = new HashMap<>();
	// Leather
	private Color leatherColor;
	// Skull
	private PlayerProfile skullOwner;
	// Potion
	private Color potionColor;
	private final Set<PotionEffect> potionEffects = new HashSet<>();
	// Compass target
	private Location compassTarget;
	// damageable
	private int itemDamages;
	
	// If null, then we're creating the item !
	private PersistentDataContainer container;
	private final Map<NamespacedKey, Collection<PersistentValue<?>>> containedValues = new HashMap<>();
	
	public ItemBuilder(Material type) {
		this(type, 1);
	}
	public ItemBuilder(Material type, int amount) {
		this.type = type;
		this.amount = amount;
	}
	public ItemBuilder(ItemStack is) {
		// Basic
		type = is.getType();
		amount = is.getAmount();
		enchants.putAll(is.getEnchantments());
		flags.addAll(is.getItemFlags());
		// Basic meta
		ItemMeta meta = is.getItemMeta();
		unbreakable = meta.isUnbreakable();
		if(meta.hasDisplayName())
			displayName = meta.getDisplayName();
		if(meta.hasLore())
			lore.addAll(meta.getLore());
		// High-end meta
		container = meta.getPersistentDataContainer();
		if(meta.hasAttributeModifiers()) {
			for(Attribute a : Attribute.values()) {
				Collection<AttributeModifier> am = meta.getAttributeModifiers(a);
				if(null != am && am.isEmpty())
					continue;
				modifiers.put(a, am);
			}
		}
		// Specific-meta
		if(meta instanceof LeatherArmorMeta) {
			leatherColor = ((LeatherArmorMeta)meta).getColor();
		} else if(meta instanceof SkullMeta && skullOwner != null) {
			if(((SkullMeta)meta).hasOwner())
				skullOwner = ((SkullMeta)meta).getPlayerProfile();
		} else if(meta instanceof PotionMeta) {
			potionColor = ((PotionMeta)meta).getColor();
			potionEffects.addAll(((PotionMeta)meta).getCustomEffects());
		} else if(meta instanceof SuspiciousStewMeta) {
			potionEffects.addAll(((SuspiciousStewMeta)meta).getCustomEffects());
		} else if(meta instanceof CompassMeta) {
			if(((CompassMeta)meta).hasLodestone())
				compassTarget = ((CompassMeta)meta).getLodestone();
		} else if(meta instanceof Damageable) {
			if(((Damageable)meta).hasDamage())
				itemDamages = ((Damageable)meta).getDamage();
		}
	}
	
	@SuppressWarnings("unchecked")
	public ItemStack build() {
		ItemStack is = new ItemStack(type, amount);
		flags.forEach(is::addItemFlags);
		is.addUnsafeEnchantments(enchants);
		ItemMeta meta = is.getItemMeta();
		meta.setUnbreakable(unbreakable);
		for(Map.Entry<Attribute,Collection<AttributeModifier>> enam : modifiers.entrySet()) {
			enam.getValue().forEach(am -> meta.addAttributeModifier(enam.getKey(), am));
		}
		if(container == null) {
			for(Map.Entry<NamespacedKey, Collection<PersistentValue<?>>> entry : containedValues.entrySet()) {
				for(PersistentValue<?> val : entry.getValue()) {
					meta.getPersistentDataContainer().set(entry.getKey(), (PersistentDataType<? extends Object, ? super Object>) val.type, (Object) val.value);
				}
			}
		}
		
		if(displayName != null)
			meta.setDisplayName(displayName);
		meta.setLore(lore);
		if(meta instanceof LeatherArmorMeta && leatherColor != null) {
			((LeatherArmorMeta)meta).setColor(leatherColor);
		} else if(meta instanceof SkullMeta && skullOwner != null) {
			((SkullMeta)meta).setOwnerProfile(skullOwner);
		} else if(meta instanceof PotionMeta) {
			if(potionColor != null)
				((PotionMeta)meta).setColor(potionColor);
			potionEffects.forEach(pe -> ((PotionMeta)meta).addCustomEffect(pe, true));
		} else if(meta instanceof SuspiciousStewMeta) {
			potionEffects.forEach(pe -> ((SuspiciousStewMeta)meta).addCustomEffect(pe, true));
		} else if(meta instanceof CompassMeta) {
			((CompassMeta)meta).setLodestone(compassTarget);
		} else if(meta instanceof Damageable) {
			((Damageable)meta).setDamage(itemDamages);
		}
		is.setItemMeta(meta);
		return is;
	}
	
	public int getAmount() {
		return amount;
	}
	public ItemBuilder setAmount(int amount) {
		this.amount = amount;
		return this;
	}
	public Material getType() {
		return type;
	}
	public ItemBuilder setType(Material type) {
		this.type = type;
		return this;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public ItemBuilder setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}
	
	public List<String> getLore() {
		return lore;
	}
	public ItemBuilder setLore(List<String> lore) {
		this.lore.clear();
		this.lore.addAll(lore);
		return this;
	}
	public ItemBuilder addLoreLine(String line) {
		lore.add(line);
		return this;
	}
	public ItemBuilder addLoreLines(List<String> lines) {
		lore.addAll(lines);
		return this;
	}
	public ItemBuilder addLoreLines(String... lines) {
		lore.addAll(List.of(lines));
		return this;
	}
	public ItemBuilder insertLoreLine(String line, int index) {
		index = Math.max(0, Math.min(lore.size(), index));
		lore.add(index, line);
		return this;
	}
	
	public Set<ItemFlag> getItemFlags() {
		return new HashSet<>(flags);
	}
	public ItemBuilder addItemFlag(ItemFlag flag) {
		flags.add(flag);
		return this;
	}
	public ItemBuilder addItemFlags(ItemFlag... flags) {
		this.flags.addAll(Set.of(flags));
		return this;
	}
	public ItemBuilder addItemFlags(Set<ItemFlag> flags) {
		this.flags.addAll(flags);
		return this;
	}
	public ItemBuilder clearItemFlags() {
		flags.clear();
		return this;
	}
	public ItemBuilder removeItemFlag(ItemFlag flag) {
		this.flags.remove(flag);
		return this;
	}
	
	public Map<Enchantment, Integer> getEnchants() {
		return new HashMap<>(enchants);
	}
	public ItemBuilder addEnchant(Enchantment enchantment, int level) {
		enchants.put(enchantment, level);
		return this;
	}
	public ItemBuilder clearEnchants() {
		enchants.clear();
		return this;
	}
	public ItemBuilder setEnchants(Map<Enchantment, Integer> enchants) {
		enchants.clear();
		this.enchants.putAll(enchants);
		return this;
	}
	public ItemBuilder addEnchants(Map<Enchantment, Integer> enchants) {
		this.enchants.putAll(enchants);
		return this;
	}
	public ItemBuilder removeEnchant(Enchantment enchantment) {
		enchants.remove(enchantment);
		return this;
	}
	
	public Color getLeatherColor() {
		return leatherColor;
	}
	
	public ItemBuilder setLeatherColor(Color leatherColor) {
		this.leatherColor = leatherColor;
		return this;
	}
	
	public ItemBuilder setPotionColor(Color potionColor) {
		this.potionColor = potionColor;
		return this;
	}
	public Color getPotionColor() {
		return potionColor;
	}
	public Set<PotionEffect> getPotionEffects() {
		return new HashSet<>(potionEffects);
	}
	public ItemBuilder addPotionEffect(PotionEffect effect) {
		potionEffects.add(effect);
		return this;
	}
	public ItemBuilder addPotionEffects(Set<PotionEffect> effects) {
		this.potionEffects.addAll(effects);
		return this;
	}
	public ItemBuilder clearPotionEffects() {
		potionEffects.clear();
		return this;
	}
	public ItemBuilder removePotionEffect(PotionEffect effect) {
		this.potionEffects.remove(effect);
		return this;
	}
	
	public ItemBuilder setSkullOwner(PlayerProfile skullOwner) {
		this.skullOwner = skullOwner;
		return this;
	}
	public PlayerProfile getSkullOwner() {
		return skullOwner;
	}
	
	public Location getCompassTarget() {
		return compassTarget;
	}
	public ItemBuilder setCompassTarget(Location target) {
		compassTarget = target;
		return this;
	}
	
	public boolean isUnbreakable() {
		return unbreakable;
	}
	public ItemBuilder setUnbreakable() {
		return setUnbreakable(true);
	}
	public ItemBuilder setUnbreakable(boolean unbreakable) {
		this.unbreakable = unbreakable;
		return this;
	}
	public int getItemDamages() {
		return itemDamages;
	}
	public ItemBuilder setItemDamages(int itemDamages) {
		this.itemDamages = itemDamages;
		return this;
	}
	
	public ItemBuilder setShining() {
		addEnchant(Enchantment.DURABILITY, 1);
		addItemFlag(ItemFlag.HIDE_ENCHANTS);
		return this;
	}
	
	public ItemBuilder addAttributeModifier(Attribute attribute, double value, AttributeModifier.Operation operation) {
		AttributeModifier am = new AttributeModifier(attribute.name(), value, operation);
		if(!modifiers.containsKey(attribute))
			modifiers.put(attribute, new HashSet<>());
		modifiers.get(attribute).add(am);
		return this;
	}
	public ItemBuilder addAttributeModifier(Attribute attribute, double value, AttributeModifier.Operation operation, EquipmentSlot slot) {
		AttributeModifier am = new AttributeModifier(UUID.randomUUID(), attribute.name(), value, operation, slot);
		if(!modifiers.containsKey(attribute))
			modifiers.put(attribute, new HashSet<>());
		modifiers.get(attribute).add(am);
		return this;
	}
	public ItemBuilder clearAttributeModifiers() {
		modifiers.clear();
		return this;
	}
	
	private static record PersistentValue<Z>(PersistentDataType<?, Z> type, Z value) {}
	
	public <T,Z> ItemBuilder setPersistentData(NamespacedKey key, PersistentDataType<T,Z> type, Z value) {
		if(container == null) {
			if(!containedValues.containsKey(key))
				containedValues.put(key, new HashSet<>());
			containedValues.get(key).add(new PersistentValue<>(type, value));
		} else {
			container.set(key, type, value);
		}
		return this;
	}
	public ItemBuilder removePersistentData(NamespacedKey key) {
		if(container == null) {
			containedValues.remove(key);
		} else {
			container.remove(key);
		}
		return this;
	}
	public ItemBuilder clearPersistentData() {
		if(container == null) {
			containedValues.clear();
		} else {
			container.getKeys().forEach(container::remove);
		}
		return this;
	}
	public <T> T getPersistentData(NamespacedKey key, PersistentDataType<?,T> type) {
		if(container == null) {
			if(!containedValues.containsKey(key))
				return null;
			return (T) containedValues.get(key).stream()
					.filter(v -> v.type().equals(type))
					.map(PersistentValue::value)
					.findFirst()
					.orElse(null);
		} else {
			return container.get(key, type);
		}
	}
	
	public static @Nullable String justReadPersistentKey(NamespacedKey key, ItemStack item) {
		String val = item.getItemMeta().getPersistentDataContainer().getOrDefault(key, PersistentDataType.STRING, "");
		return val.isEmpty() ? null : val;
	}
	
}
