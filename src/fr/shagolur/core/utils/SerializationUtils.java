package fr.shagolur.core.utils;

import fr.shagolur.core.entities.BasicStatsHolder;
import fr.shagolur.core.entities.MonsterData;
import fr.shagolur.core.entities.StatsHolder;
import fr.shagolur.core.items.Data;
import fr.shagolur.core.items.DataModifier;
import fr.shagolur.core.items.McEnchantment;
import fr.shagolur.core.items.Modifier;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.enchantments.Enchantment;
import org.jetbrains.annotations.NotNull;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public final class SerializationUtils {
	private SerializationUtils() {}
	
	private final static String S = ";";
	
	private static String ft(float val) {
		return String.format("%.02f", val);
	}
	private static String ft(double val) {
		return String.format("%.02f", val);
	}
	
	public static String modifsToString(Set<DataModifier> modifs) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(DataModifier dm : modifs) {
			if(first) first = false; else sb.append(S);
			sb.append(dm.data().serialize())
					.append(",")
					.append(dm.modifier().serialize())
					.append(",")
					.append(ft(dm.value()));
		}
		return sb.toString();
	}
	public static Set<DataModifier> stringToModifiers(String txt) {
		Set<DataModifier> modifs = new HashSet<>();
		for(String modif : txt.split(S)) {
			String[] tokens = modif.split(",");
			modifs.add(new DataModifier(
					Data.deserialize(tokens[0]),
					Modifier.deserialize(tokens[1]),
					Float.parseFloat(tokens[2])
			));
		}
		return modifs;
	}
	
	public static String loreToString(List<String> lore) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(String l : lore) {
			if(first) first = false; else sb.append("\\n");
			sb.append(l); //TODO pas bon, il faut virer les color code & retirer le premier gris.
		}
		return sb.toString();
	}
	public static @NotNull List<String> stringToLore(String txt) {
		if(txt == null || txt.isBlank())
			return Collections.emptyList();
		return Stream.of(txt.replace("\r", "").replace("\\r", "").replace("\\n", "\n").split("\n"))
				.map(l -> ChatColor.DARK_GRAY + ChatColor.translateAlternateColorCodes('&', l))
				.toList();
	}
	
	public static String locationToString(Location location) {
		return location.getWorld().getName() + S
				+ ft(location.getX()) + S
				+ ft(location.getY()) + S
				+ ft(location.getZ()) + S
				+ ft(location.getYaw()) + S
				+ ft(location.getPitch());
	}
	
	public static Location stringToLocation(String txt) {
		String[] tokens = txt.split(S);
		return new Location(
				Bukkit.getWorld(tokens[0]),
				Double.parseDouble(tokens[1]),
				Double.parseDouble(tokens[2]),
				Double.parseDouble(tokens[3]),
				Float.parseFloat(tokens[4]),
				Float.parseFloat(tokens[5])
		);
	}
	
	public static Set<McEnchantment> stringToEnchants(String txt) {
		Set<McEnchantment> enchantments = new HashSet<>();
		for(String modif : txt.split(S)) {
			String[] tokens = modif.split(",");
			enchantments.add(new McEnchantment(
					Enchantment.getByKey(NamespacedKey.minecraft(tokens[0])),
					Integer.parseInt(tokens[1])
			));
		}
		return enchantments;
	}
	public static String enchantsToString(Set<McEnchantment> enchantments) {
		StringBuilder sb = new StringBuilder();
		boolean first = true;
		for(McEnchantment enchantment : enchantments) {
			if(first) first = false; else sb.append(S);
			sb.append(enchantment.enchantment().getKey())
					.append(",")
					.append(enchantment.level());
		}
		return sb.toString();
	}
	
	public static BasicStatsHolder stringToStats(String txt) {
		BasicStatsHolder bh = new BasicStatsHolder();
		if(txt == null)
			return bh;
		String[] tokens = txt.split(S);
		bh.setMaxHealth(Float.parseFloat(tokens[0]));
		bh.setMaxMana(Float.parseFloat(tokens[1]));
		bh.setPhysicalDamage(Float.parseFloat(tokens[2]));
		bh.setMagicalDamage(Float.parseFloat(tokens[3]));
		bh.setPhysicalResistance(Float.parseFloat(tokens[4]));
		bh.setMagicalResistance(Float.parseFloat(tokens[5]));
		bh.setLevel(Integer.parseInt(tokens[6]));
		return bh;
	}
	public static String statsToString(StatsHolder stats) {
		return ft(stats.getMaxHealth())+S
				+ ft(stats.getMaxMana())+S
				+ ft(stats.getPhysicalDamage())+S
				+ ft(stats.getMagicalDamage())+S
				+ ft(stats.getPhysicalResistance())+S
				+ ft(stats.getMagicalResistance())+S
				+ stats.getLevel();
	}
	
	public static String niceLocation(Location loc, String color) {
		if(loc == null)
			return ChatColor.GRAY + "" + ChatColor.ITALIC + "(undefined)";
		return "("+(int)loc.getX()+", "+(int)loc.getY()+", " + (int)loc.getZ() + ")[§7"+loc.getWorld().getName()+color+"]";
	}
	
	// flags = <data>;<data>;...
	public static void applyFlagsToMonsterData(String flags, MonsterData monsterData) {
		if(flags == null)
			return;
		for(String flag : flags.split(";")) {
			if(flag.equals("baby"))
				monsterData.setBabie();
			else if(flag.equals("invi"))
				monsterData.setInvisible();
		}
	}
	public static String extractFlags(MonsterData monsterData) {
		String flags = "";
		if(monsterData.isBabie())
			flags += "baby;";
		if(monsterData.isInvisible())
			flags += "invi;";
		return flags;
	}
	
}
