package fr.shagolur.core.listeners;

import com.destroystokyo.paper.event.player.PlayerArmorChangeEvent;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.utils.StupidSerializer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerAttemptPickupItemEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

public class BukkitArmorEquipListener extends JamListener {
	
	@EventHandler
	public void playerSwapItem(PlayerSwapHandItemsEvent e) {
		SPlayer p = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		p.recalculateEquipment();
	}
	
	@EventHandler
	public void playerHotbarSwap(PlayerItemHeldEvent e) {
		SPlayer p = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		ItemStack is = e.getPlayer().getInventory().getItem(e.getNewSlot());
		p.updatePartEquipment(EquipmentSlot.HAND, ItemData.convert(is));
	}
	
	@EventHandler
	public void playerEquipArmor(PlayerArmorChangeEvent e) {
		SPlayer p = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		p.getEquipment().updatePart(
				StupidSerializer.slotTypeToEquipmentSlot(e.getSlotType()),
				ItemData.convert(e.getNewItem())
		);
		ItemStack is = e.getPlayer().getEquipment().getItemInMainHand();
		p.updatePartEquipment(EquipmentSlot.HAND, ItemData.convert(is));
	}
	
	@EventHandler
	public void playerDropItem(PlayerDropItemEvent e) {
		SPlayer p = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		ItemStack is = e.getPlayer().getEquipment().getItemInMainHand();
		p.updatePartEquipment(EquipmentSlot.HAND, ItemData.convert(is));
	}
	
	@EventHandler
	public void playerPickupItem(PlayerAttemptPickupItemEvent e) {
		SPlayer p = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		//TODO à améliorer... Mais ya aucun event qui fonctionne bien....
		if(!e.isCancelled()) {
			Bukkit.getScheduler().runTaskLater(Shagolur.plugin(), () -> {
				ItemStack is = p.getPlayer().getEquipment().getItemInMainHand();
				p.updatePartEquipment(EquipmentSlot.HAND, ItemData.convert(is));
			}, 10L);
		}
	}
	
	
	
}
