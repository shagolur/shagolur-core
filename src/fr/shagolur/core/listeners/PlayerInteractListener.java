package fr.shagolur.core.listeners;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.MonsterData;
import fr.shagolur.core.utils.ItemBuilder;
import org.bukkit.GameMode;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.jetbrains.annotations.NotNull;

public class PlayerInteractListener extends JamListener {
	
	@EventHandler
	public void playerInteractsEvent(@NotNull PlayerInteractEvent e) {
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			if(e.getItem() != null) {
				String spawnVal = ItemBuilder.justReadPersistentKey(MonsterData.keyContainsMob(), e.getItem());
				if(spawnVal != null) {
					assert e.getInteractionPoint() != null;
					Shagolur.getEntitiesManager().spawn(spawnVal, e.getInteractionPoint());
					if(e.getPlayer().getGameMode() != GameMode.CREATIVE)
						e.getItem().setAmount(e.getItem().getAmount() - 1);
					e.setUseInteractedBlock(Event.Result.DENY);
					e.setUseItemInHand(Event.Result.DENY);
				}
			}
		}
	}
}
