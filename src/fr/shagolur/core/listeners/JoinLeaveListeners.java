package fr.shagolur.core.listeners;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.events.ShagolurPlayerJoinEvent;
import fr.shagolur.core.events.ShagolurPlayerQuitEvent;
import fr.shagolur.core.players.SPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class JoinLeaveListeners extends JamListener {
	
	@EventHandler
	public void playerJoinEvent(PlayerJoinEvent e) {
		e.setJoinMessage("");
		Bukkit.broadcastMessage(ChatColor.GRAY+"["+ChatColor.GREEN+"+"+ChatColor.GRAY+"] Le joueur " + ChatColor.YELLOW + e.getPlayer().getName() + ChatColor.GRAY + " a rejoint le serveur.");
		// First connect the player
		SPlayer player = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		player.connect(e.getPlayer());
		// Then propagate the event
		Bukkit.getPluginManager().callEvent(new ShagolurPlayerJoinEvent(player));
	}
	
	@EventHandler
	public void playerQuitEvent(PlayerQuitEvent e) {
		e.setQuitMessage("");
		Bukkit.broadcastMessage(ChatColor.GRAY+"["+ChatColor.RED+"-"+ChatColor.GRAY+"] Le joueur " + ChatColor.YELLOW + e.getPlayer().getName() + ChatColor.GRAY + " a quitté le serveur.");
		// First, propagate the event.
		SPlayer player = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		Bukkit.getPluginManager().callEvent(new ShagolurPlayerQuitEvent(player));
		// Then disconnect it
		player.disconnect();
		// Then save it
		Shagolur.getPlayersManager().save(player);
	}
	
}
