package fr.shagolur.core.listeners;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.players.SPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class BukkitDeathListeners extends JamListener {
	
	@EventHandler
	public void entityDeathEvent(@NotNull EntityDeathEvent e) {
		if(e.getEntity() instanceof Player)
			return;
		// Clear exp & items drop
		e.setDroppedExp(0);
		e.getDrops().clear();
		// The rest is handled by the class SEntity
	}
	
	private final Set<UUID> overflow = new HashSet<>();
	
	@EventHandler
	public void playerDeathEvent(@NotNull PlayerDeathEvent e) {
		// The event is called two times...
		e.setDeathMessage("");
		e.setDroppedExp(0);
		e.getDrops().clear();
		e.setKeepInventory(true);
		e.setKeepLevel(true);
		if(!overflow.add(e.getPlayer().getUniqueId()))
			return;
		// Then call the #respawn of player
		SPlayer player = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		Bukkit.getScheduler().runTaskLater(Shagolur.plugin(), () -> {
			player.respawn();
			overflow.remove(e.getPlayer().getUniqueId());
		}, 15L);
	}

}
