package fr.shagolur.core.listeners;

import io.papermc.paper.event.entity.EntityDamageItemEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.jetbrains.annotations.NotNull;

public class BukkitNaturalListeners extends JamListener {
	
	@EventHandler
	public void healthRegenEvent(@NotNull EntityRegainHealthEvent e) {
		e.setCancelled(true);
	}
	
	
	@EventHandler
	public void itemDamage(@NotNull EntityDamageItemEvent e) {
		// We cancel all damages to items, no need for them to be unbreakable.
		e.setCancelled(true);
	}

}
