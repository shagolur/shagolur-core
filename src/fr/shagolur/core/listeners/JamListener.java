package fr.shagolur.core.listeners;

import fr.shagolur.core.Shagolur;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public abstract class JamListener implements Listener {
	
	protected final Shagolur main;
	
	public JamListener() {
		main = Shagolur.plugin();
		Bukkit.getPluginManager().registerEvents(this, Shagolur.plugin());
	}
	
}
