package fr.shagolur.core.listeners;

import com.destroystokyo.paper.event.player.PlayerAttackEntityCooldownResetEvent;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.events.ShagolurDamageEvent;
import fr.shagolur.core.players.SPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.jetbrains.annotations.NotNull;

public class BukkitDamageListeners extends JamListener {
	
	@EventHandler
	public void playerAttackEvent(PlayerAttackEntityCooldownResetEvent e) {
		SPlayer damager = Shagolur.getPlayersManager().getPlayer(e.getPlayer());
		SEntity victim = Shagolur.getEntitiesManager().getSEntity(e.getAttackedEntity());
		if(victim == null) {
			Shagolur.warning("Unregistered victim ("+e.getAttackedEntity().getUniqueId()+") just got damaged BY §dplayer§r " + damager.getName());
			e.getAttackedEntity().remove();
			return;
		}
		double damages = damager.getAttackValue() * e.getCooledAttackStrength();
		doDamages(victim, damager, damages, ! damager.isAttackMagical(), e);
	}
	
	@EventHandler
	public void entityDamageEvent(EntityDamageByEntityEvent e) {
		// Revert bukkit damages, to keep the knockback effect
		if(e.getDamager() instanceof Player) {
			e.setDamage(0);
			return; // Si attaquant est un joueur, on laisse faire la méthode spécialisée pour ça
		}
		if(e.getCause() == EntityDamageEvent.DamageCause.FALLING_BLOCK || e.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION)
			return; // Géré par l'environnement.
		e.setDamage(0);
		// Get various data
		SEntity damager = Shagolur.getEntitiesManager().getSEntity(e.getDamager());
		if(damager == null) {
			Shagolur.warning("Unregistered entity ("+e.getDamager().getUniqueId()+") just damaged " + e.getEntity().getName());
			return;
		}
		SEntity victim = Shagolur.getEntitiesManager().getSEntity(e.getEntity());
		if(victim == null) {
			Shagolur.warning("Unregistered victim ("+e.getDamager().getUniqueId()+") just got damaged BY " + damager.getName());
			return;
		}
		doDamages(victim, damager, damager.getAttackValue(), ! damager.isAttackMagical(), e);
	}
	
	@EventHandler
	public void entityDamageEnvironmental(@NotNull EntityDamageEvent e) {
		if(e.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK
				|| e.getCause() == EntityDamageEvent.DamageCause.ENTITY_SWEEP_ATTACK
				|| e.getCause() == EntityDamageEvent.DamageCause.CUSTOM) // CUSTOM == suicide par plugin.
			return;
		SEntity victim = Shagolur.getEntitiesManager().getSEntity(e.getEntity());
		if(victim == null) {
			Shagolur.warning("COuld not find entity corresponding to ["+e.getEntity().getUniqueId()+" | " + e.getEntity().getName()+"] : suppressing entity.");
			e.getEntity().remove();
			e.setCancelled(true);
			return;
		}
		//Bukkit.broadcastMessage("§e("+victim.getName()+"§e) damage : §b"+e.getCause()+"§e, dmg=§c"+e.getDamage());
		double multiplier = 1.0;
		boolean physical = true;
		switch(e.getCause()) {
			case ENTITY_SWEEP_ATTACK, ENTITY_ATTACK, PROJECTILE, CUSTOM -> {return;}
			case SUICIDE, VOID -> {
				e.setCancelled(true);
				victim.die();
				return;
			}
			case STARVATION -> {
				e.setCancelled(true);
				return;
			}
			case FALL -> {
				multiplier = 1.5;
			}
			case DRAGON_BREATH -> {
				multiplier = 10;
				physical = false;
			}
			case LAVA, DROWNING, SUFFOCATION -> {
				multiplier = 5;
			}
			case LIGHTNING -> {
				multiplier = 8;
				physical = false;
			}
			case WITHER, POISON, MAGIC -> {
				multiplier = 4;
				physical = false;
			}
		}
		double damages = e.getDamage() * multiplier * 5.0;
		e.setDamage(0); // 0 vanilla damages
		//TODO pour certains sorts, va falloir rechoper qui a fait les dégats, et comment
		doDamages(victim, null, damages, physical, e);
	}
	
	private void doDamages(SEntity victim, SEntity damager, double damages, boolean physical, Cancellable e) {
		// Propagate event
		ShagolurDamageEvent event = new ShagolurDamageEvent(victim, damager, physical, damages);
		Bukkit.getPluginManager().callEvent(event);
		//Bukkit.broadcastMessage("new damage event : " + event);
		if(event.isCancelled()) {
			e.setCancelled(true);
			return;
		}
		// Apply damages on SEntities
		if(damager == null)
			victim.damage(damages, physical);
		else
			victim.damageBy(damager, damages, physical);
	}
	

}
