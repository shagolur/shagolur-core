package fr.shagolur.core.listeners;

import fr.shagolur.core.utils.MenuGUI;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.jetbrains.annotations.NotNull;

public class GuiListeners extends JamListener {
	
	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void onInventoryClose(@NotNull InventoryCloseEvent e){
		MenuGUI.checkForMenuClose(main, e);
	}
	
	@EventHandler
	public void onInventoryClick(@NotNull InventoryClickEvent e){
		if( ! (e.getWhoClicked() instanceof Player))
			return;
		MenuGUI menu = MenuGUI.checkForMenuClick(main, e, true);
		if(menu != null){
			e.setCancelled(true);
			((Player) e.getWhoClicked()).updateInventory();
		}
	}
	
}
