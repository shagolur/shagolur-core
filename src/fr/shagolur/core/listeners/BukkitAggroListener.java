package fr.shagolur.core.listeners;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.SEntity;
import fr.shagolur.core.events.ShagolurCanAggroRequest;
import fr.shagolur.core.players.SPlayer;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityTargetEvent;
import org.jetbrains.annotations.NotNull;

public class BukkitAggroListener extends JamListener {
	
	@EventHandler
	public void mobAggro(@NotNull EntityTargetEvent e) {
		SEntity entity = Shagolur.getEntitiesManager().getSEntity(e.getEntity());
		if(entity == null) {
			Shagolur.warning("Entity " + e.getEntity() + " could NOT have shagolur Entity in target event. Remove it.");
			e.setCancelled(true);
			e.getEntity().remove();
			return;
		}
		// 1) Si entité n'a PAS d'owner (ou qu'il n'y a pas de cible), on laisse faire
		if(!entity.hasOwner() || e.getTarget() == null)
			return;
		SEntity target = Shagolur.getEntitiesManager().getSEntity(e.getTarget());
		if(target == null)
			return;
		
		// Si les deux ont le même owner / l'un est celui de l'autre, on cancel
		if(entity.hasOwner() && entity.getRootOwner().getUUID().equals(target.getRootOwner().getUUID())) {
			e.setCancelled(true);
			return;
		}
		// Si owner de l'entité est un joueur, on veille à ce qu'il ne puisse PAS aggro ses alliés
		if(entity.getRootOwner() instanceof SPlayer) {
			ShagolurCanAggroRequest requ = new ShagolurCanAggroRequest((SPlayer) entity.getRootOwner(), entity, target);
			Bukkit.getPluginManager().callEvent(requ);
			// Set the cancel != can hurt
			e.setCancelled(! requ.canHurt());
			//Bukkit.broadcastMessage("§e§othrew event. canceled ?" + (!requ.canHurt()));
		}
	}

}
