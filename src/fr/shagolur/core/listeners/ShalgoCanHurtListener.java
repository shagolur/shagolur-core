package fr.shagolur.core.listeners;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.events.ShagolurCanHurtRequest;
import org.bukkit.event.EventHandler;
import org.jetbrains.annotations.NotNull;

public class ShalgoCanHurtListener extends JamListener {
	
	@EventHandler
	public void canPlayerHurtRequest(@NotNull ShagolurCanHurtRequest r) {
		if(r.canHurt()) {
			if(Shagolur.getPartiesManager().areInSameParty(r.getA(), r.getB()))
				r.setCanHurt(false);
		}
	}
	
}
