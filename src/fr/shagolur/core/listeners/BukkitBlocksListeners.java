package fr.shagolur.core.listeners;

import org.bukkit.GameMode;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.jetbrains.annotations.NotNull;

public class BukkitBlocksListeners extends JamListener {
	
	@EventHandler(ignoreCancelled = true)
	public void blockPlaced(@NotNull BlockPlaceEvent e) {
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE)
			e.setCancelled(true);
	}
	
	
	@EventHandler(ignoreCancelled = true)
	public void blockBreak(@NotNull BlockBreakEvent e) {
		if(e.getPlayer().getGameMode() != GameMode.CREATIVE)
			e.setCancelled(true);
	}

}
