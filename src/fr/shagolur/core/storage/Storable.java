package fr.shagolur.core.storage;

/**
 * A class which can be serialized and persisted in a datasource.
 * @see StorageSystem
 */
public interface Storable {
	
	/**
	 * Get the primary key of this object.
	 * @return an Object, unique for the class type.
	 */
	Object storageID();
	
	/**
	 * Clone current data to a serializable object.
	 * @return a serializable object
	 */
	Serializer<?> serialize();
	
	/**
	 * @return true if an instance of this object (sharing the #storageID) already exists
	 */
	boolean alreadyInBDD();
	
	void declarePersistentInBDD();
	
}
