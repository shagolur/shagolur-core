package fr.shagolur.core.storage.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.shagolur.core.Shagolur;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

record APIManager(@NotNull String baseUrl, @Nullable String token) {
	
	private @NotNull String queryToken() {
		if(token == null || token.isEmpty())
			return "";
		return "?token="+token;
	}
	
	private boolean isValid(int status) {
		return status >= 200 && status < 300;
	}
	
	public boolean post(String output, Object content) {
		String url = baseUrl + output + queryToken();
		try {
			HttpPost post = new HttpPost(url);
			if (content != null) {
				Gson gson = new GsonBuilder().create();
				StringEntity postingString = new StringEntity(gson.toJson(content));
				post.setEntity(postingString);
			}
			HttpResponse response = execute(post);
			return isValid(response.getStatusLine().getStatusCode());
		} catch (IOException e) {
			Shagolur.error(e.getLocalizedMessage());
			return false;
		}
	}
	
	public <T> T post(String output, Class<T> responseType) {
		return post(output, null, responseType);
	}
	
	public <T> T post(String output, Object content, Class<T> responseType) { // object = une instance avec que des fields à la con genre des strings et tout
		String url = baseUrl + output + queryToken();
		try {
			HttpPost post = new HttpPost(url);
			if (content != null) {
				Gson gson = new GsonBuilder().create();
				StringEntity postingString = new StringEntity(gson.toJson(content));
				Shagolur.debug("POST : " + gson.toJson(content));
				post.setEntity(postingString);
			}
			HttpResponse response = execute(post);
			if ( ! isValid(response.getStatusLine().getStatusCode())) {
				Shagolur.error("Error (" + response.getStatusLine().getStatusCode() + ") on POST to '" + url + "' : " + response.getStatusLine().getReasonPhrase() + ".");
				return null;
			}
			return getResponse(response, responseType);
		} catch (IOException e) {
			Shagolur.error(e.getLocalizedMessage());
		}
		return null;
	}
	
	public <T> T get(String output, Class<T> responseType) {
		return get(output, responseType, false);
	}
	
	public <T> T get(String output, Class<T> responseType, boolean accepts404) {
		String url = baseUrl + output + queryToken();
		Shagolur.debug("get URL=" + url);
		try {
			HttpGet get = new HttpGet(url);
			HttpResponse response = execute(get);
			if ( ! isValid(response.getStatusLine().getStatusCode())) {
				if(!accepts404)
					Shagolur.error("Error (" + response.getStatusLine().getStatusCode() + ") on GET to '" + url + "' : " + response.getStatusLine().getReasonPhrase() + ".");
				return null;
			}
			return getResponse(response, responseType);
		} catch (IOException e) {
			Shagolur.error(e.getLocalizedMessage());
		}
		return null;
	}
	
	public boolean get(String output) {
		try {
			HttpResponse response = execute(new HttpGet(baseUrl + output + queryToken()));
			return isValid(response.getStatusLine().getStatusCode());
		} catch (IOException e) {
			Shagolur.error(e.getLocalizedMessage());
			return false;
		}
	}
	
	private boolean putPatch(String url, boolean put, Object content) {
		HttpEntityEnclosingRequestBase requ = put ? new HttpPut(url) : new HttpPatch(url);
		Shagolur.debug((put?"PUT":"PATCH") + " to " + url);
		try {
			String json = "<EMPTY CONTENT>";
			if (content != null) {
				Gson gson = new GsonBuilder().create();
				json = gson.toJson(content);
				Shagolur.debug((put?"PUT":"PATCH") + " : " + gson.toJson(content));
				StringEntity postingString = new StringEntity(json);
				requ.setEntity(postingString);
			}
			HttpResponse response = execute(requ);
			Shagolur.debug("PUT Response : " + response.getStatusLine());
			if(!isValid(response.getStatusLine().getStatusCode())) {
				Shagolur.error("ERROR during "+(put?"PUT":"PATCH")+". url="+url+", body="+json);
				Shagolur.error("error="+response.getStatusLine().getStatusCode());
				return false;
			}
			return true;
		} catch (IOException e) {
			Shagolur.error(e.getLocalizedMessage());
			return false;
		}
	}
	
	public boolean put(String output, Object content) {
		String url = baseUrl + output + queryToken();
		return putPatch(url, true, content);
	}
	
	public boolean patch(String output, Object content) {
		String url = baseUrl + output + queryToken();
		return putPatch(url, false, content);
	}
	
	private HttpResponse execute(HttpRequestBase request) throws IOException {
		request.setHeader("Content-type", "application/json");
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		return httpClient.execute(request);
	}
	
	private <T> T getResponse(HttpResponse response, Class<T> type) {
		try (BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()))) {
			StringBuilder builder = new StringBuilder();
			String line;
			while ((line = reader.readLine()) != null)
				builder.append(line);
			return new GsonBuilder().create().fromJson(builder.toString(), type);
		} catch (Exception e) {
			Shagolur.error(e.getLocalizedMessage());
			return null;
		}
	}
	
}
