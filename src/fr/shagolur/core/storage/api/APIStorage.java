package fr.shagolur.core.storage.api;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.core.storage.Storable;
import fr.shagolur.core.storage.StorageSystem;
import fr.shagolur.core.utils.Utils;

import java.nio.file.ProviderNotFoundException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class APIStorage implements StorageSystem {
	
	private final APIManager api;
	
	private final Map<Class<? extends Storable>, PathProvider<? extends Storable>> pathProviders = new HashMap<>();
	
	public APIStorage(String url, String token) {
		api = new APIManager(url, token);
	}
	
	@Override
	public void registerPathProvider(PathProvider<? extends Storable> pathProvider) {
		pathProviders.put(pathProvider.getHandledClass(), pathProvider);
		Shagolur.log("[StorageAPI] New provider registered for class '"+pathProvider.getHandledClass().getSimpleName()+"' : '"+pathProvider.getClass().getSimpleName()+"'.");
	}
	
	private PathProvider<? extends Storable> getPathProvider(Class<? extends Storable> clazz) {
		PathProvider<? extends Storable> provider = pathProviders.getOrDefault(clazz, null);
		if(provider == null) {
			throw new ProviderNotFoundException("Undefined provider for class " + clazz.getSimpleName() + ".");
		}
		if(provider.getSerializerClass() == null) {
			throw new NullPointerException("Undefined serializer for provider " + provider.getClass().getSimpleName() + ".");
		}
		return provider;
	}
	
	@Override
	public <T extends Storable> boolean hasUnique(Class<T> clazz, Object... params) {
		PathProvider<? extends Storable> provider = getPathProvider(clazz);
		return api.get(provider.getPath(params), provider.getSerializerClass(), true) != null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T extends Storable> T getUnique(Class<T> clazz, boolean allows404, Object... params) {
		PathProvider<? extends Storable> provider = getPathProvider(clazz);
		Serializer<T> serializer = (Serializer<T>) api.get(provider.getPath(params), provider.getSerializerClass(), allows404);
		if(serializer == null) {
			if(!allows404)
				Shagolur.error("No data found for " + clazz.getSimpleName() + " with params " + Arrays.toString(params));
			return null;
		}
		return serializer.deserialize();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <T extends Storable> List<T> getMultiple(Class<T> clazz, Object... params) {
		PathProvider<? extends Storable> provider = getPathProvider(clazz);
		Class<? extends Serializer<T>> serializerClass = (Class<? extends Serializer<T>>) provider.getSerializerClass();
		Class<? extends Serializer<T>[]> serializerClassArray = Utils.getArrayClass(serializerClass);
		Serializer<T>[] serializersArray = api.get(provider.getPath(params), serializerClassArray);
		if(serializersArray == null) {
			Shagolur.warning("Null get result. Class="+clazz+", params="+ Arrays.toString(params)+".");
			return Collections.emptyList();
		}
		return Stream.of(serializersArray).map(Serializer::deserialize).toList();
	}
	
	@Override
	public <T extends Storable> void saveUnique(Class<T> clazz, T object) {
		PathProvider<? extends Storable> provider = getPathProvider(clazz);
		Object serialized = object.serialize();
		if(object.alreadyInBDD()) {
			// Existe déjà : on fait un PUT sur /machin/<id>
			String path = provider.getPath(object);
			api.put(path, serialized);
		} else { // N'existe pas, on fait un POST sur /machin/
			String path = provider.getPath();
			api.post(path, serialized);
			object.declarePersistentInBDD();
		}
	}
	
	@Override
	public <T extends Storable> void saveMultiple(Class<T> clazz, List<T> object) {
	
	}
}
