package fr.shagolur.core.storage;

import fr.shagolur.core.Shagolur;

import javax.annotation.Nullable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Stream;

public abstract class StorableManager<T extends Storable> {
	
	protected final StorablesCollection<T> collection = new StorablesCollection<>();
	protected final StorageSystem storage;
	
	public StorableManager() {
		storage = Shagolur.getStorage();
		storage.registerPathProvider(getProvider());
		reloadFromBase();
	}
	
	public @Nullable T get(Object key) {
		return collection.get(key);
	}
	
	protected abstract PathProvider<T> getProvider();
	protected abstract String getStoredTypeName();
	
	public Set<T> getAll() {
		return new HashSet<>(collection.collect());
	}
	
	public Stream<T> getStream() {
		return collection.stream();
	}
	
	public void reloadFromBase() {
		Shagolur.log("Fetching "+getStoredTypeName()+" from storage.");
		collection.clear();
		collection.addAll(
				storage.getMultiple(getProvider().getHandledClass())
		);
		Shagolur.log("Fetching of "+getStoredTypeName()+" is over. Got "+collection.size()+" "+getStoredTypeName()+" from storage.");
	}
	
}
