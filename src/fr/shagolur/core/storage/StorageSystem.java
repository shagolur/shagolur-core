package fr.shagolur.core.storage;

import org.bukkit.configuration.ConfigurationSection;

import java.util.List;

/**
 * The storage system of the plugin.
 * Can be factorized with {@link StorageFactory#factory(ConfigurationSection)} and will handle {@link Storable storables} objects.
 * @see Storable
 * @see PathProvider
 */
public interface StorageSystem {
	
	/**
	 * Register a new {@link PathProvider}. It's required to use {@link #getUnique(Class, boolean, Object...) #get} and {@link #saveUnique(Class, Storable) #save} methods.
	 * @param pathProvider a PathProvider, providing for a specific object.
	 */
	void registerPathProvider(PathProvider<? extends Storable> pathProvider);
	
	<T extends Storable> boolean hasUnique(Class<T> clazz, Object... params);
	
	/**
	 * Get an instance of a {@link Storable storable} object.
	 * @param clazz the class of the object.
	 * @param params the parameter to get the exact path of the object.
	 * @return the object
	 */
	<T extends Storable> T getUnique(Class<T> clazz, boolean allows404, Object... params);
	
	/**
	 * Get an List of instances of a {@link Storable storable} object.
	 * @param clazz the class of the object.
	 * @param params the parameter to get the exact path of the object.
	 * @return the object
	 */
	<T extends Storable> List<T> getMultiple(Class<T> clazz, Object... params);
	
	<T extends Storable> void saveUnique(Class<T> clazz, T object);
	<T extends Storable> void saveMultiple(Class<T> clazz, List<T> object);
	
}
