package fr.shagolur.core.storage;

/**
 * Classe providing a datasource path.
 * @param <T> a storable class
 * @see Storable
 */
public interface PathProvider<T extends Storable> {
	
	/**
	 * The class handled by this PathProvider. Mandatary to compensate the type erasure from java.
	 * @return the provided class.
	 */
	Class<T> getHandledClass();
	
	/**
	 * Get the class which serialize the storable class.
	 * @return a {@link Serializer} class
	 */
	Class<? extends Serializer<T>> getSerializerClass();
	
	/**
	 * Get the path corresponding to the type, depending of the parameters.
	 * @param params the paramters, depends along the type.
	 * @return a path used to read/write in the datasource
	 */
	String getPath(Object... params);

}
