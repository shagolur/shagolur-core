package fr.shagolur.core.storage;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class StorablesCollection<T extends Storable> implements Iterable<T> {
	
	private final Map<Object, T> objects;
	
	public StorablesCollection() {
		this.objects = new HashMap<>();
	}
	
	public void clear() {
		objects.clear();
	}
	
	public void add(T object) {
		objects.put(object.storageID(), object);
	}
	public void addAll(Collection<T> elements) {
		elements.forEach(this::add);
	}
	
	public @Nullable T get(Object bddID) {
		return objects.getOrDefault(bddID, null);
	}
	
	public Stream<T> stream() {return objects.values().stream();}
	
	public int size() {
		return objects.size();
	}
	
	public Collection<T> collect() {
		return Collections.unmodifiableCollection(objects.values());
	}
	
	@NotNull
	@Override
	public Iterator<T> iterator() {
		return List.copyOf(objects.values()).iterator();
	}
}
