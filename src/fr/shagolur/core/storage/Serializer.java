package fr.shagolur.core.storage;

public interface Serializer<T extends Storable> {
	
	T deserialize();
	
}
