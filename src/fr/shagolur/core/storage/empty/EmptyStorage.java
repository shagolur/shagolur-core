package fr.shagolur.core.storage.empty;

import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.Storable;
import fr.shagolur.core.storage.StorageSystem;

import java.util.Collections;
import java.util.List;

public class EmptyStorage implements StorageSystem {
	
	@Override
	public void registerPathProvider(PathProvider<? extends Storable> pathProvider) {}
	
	@Override
	public <T extends Storable> boolean hasUnique(Class<T> clazz, Object... params) {
		return false;
	}
	
	@Override
	public <T extends Storable> T getUnique(Class<T> clazz, boolean allows404, Object... params) {
		return null;
	}
	
	@Override
	public <T extends Storable> List<T> getMultiple(Class<T> clazz, Object... params) {
		return Collections.emptyList();
	}
	
	@Override
	public <T extends Storable> void saveUnique(Class<T> clazz, T object) {}
	
	@Override
	public <T extends Storable> void saveMultiple(Class<T> clazz, List<T> object) {}
}
