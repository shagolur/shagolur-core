package fr.shagolur.core.storage.common;

import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.Serializer;
import java.util.Arrays;

/**
 * PathProvider for an ItemData class.
 * To obtain the path, can give an ItemData or it's id (as a string).
 * @see PathProvider
 * @see ItemData
 */
public class ItemDataPathProvider implements PathProvider<ItemData> {
	
	public static final String PATH = "core/items/";
	
	@Override
	public Class<ItemData> getHandledClass() {
		return ItemData.class;
	}
	
	@Override
	public Class<? extends Serializer<ItemData>> getSerializerClass() {
		return ItemData.ItemDataBDD.class;
	}
	
	@Override
	public String getPath(Object... params) {
		if(params.length == 0)
			return PATH;
		if(params.length != 1)
			throw new RuntimeException("ItemDataPathProvider only allow 1 parameter : String (item data id)/ItemData. Provided " + Arrays.toString(params));
		if(params[0] instanceof String)
			return PATH + params[0] + "/";
		if(params[0] instanceof ItemData)
			return PATH + ((ItemData)params[0]).getId() + "/";
		throw new RuntimeException("ItemDataPathProvider only allow 1 parameter : String (item data id)/ItemData. Provided " + params[0]);
	}
}
