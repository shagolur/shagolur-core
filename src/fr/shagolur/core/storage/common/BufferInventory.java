package fr.shagolur.core.storage.common;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.items.ItemData;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BufferInventory {
	
	private final Map<Integer, ItemStack> inventoryContent = new HashMap<>();
	
	private final List<ItemInventorySerialized> serializedInv = new ArrayList<>();
	
	public BufferInventory() {}
	
	public ItemInventorySerialized[] toInventorySerialized() {
		return serializedInv.toArray(new ItemInventorySerialized[0]);
	}
	
	public BufferInventory(ItemInventorySerialized[] inv) {
		for(ItemInventorySerialized item : inv) {
			ItemData data = Shagolur.getItemsManager().get(item.item);
			if(data != null) {
				inventoryContent.put(item.slot,data.toItemStack(item.amount));
			} else {
				Shagolur.error("Unknown item value in inventory : \"" + item.item + "\".");
			}
		}
	}
	
	public void exportToInventory(PlayerInventory inventory) {
		Bukkit.broadcastMessage("IMPORT FROM REMOTE : ");
		for(int i : inventoryContent.keySet()) {
			Bukkit.broadcastMessage("["+i+"] = " + inventoryContent.get(i).getItemMeta().getDisplayName());
			inventory.setItem(i, inventoryContent.get(i));
		}
	}
	
	public void clear() {
		inventoryContent.clear();
		serializedInv.clear();
	}
	
	public void importFromInventory(PlayerInventory inventory) {
		clear();
		Bukkit.broadcastMessage("EXPORT TO REMOTE : ");
		for(int i = 0; i < inventory.getSize(); i++) {
			ItemStack is = inventory.getItem(i);
			if(is != null) {
				Bukkit.broadcastMessage("["+i+"] = " + is.getItemMeta().getDisplayName());
				String data = ItemData.convertViaID(is);
				if(data != null) {
					ItemInventorySerialized ser = new ItemInventorySerialized(data, is.getAmount(), i);
					serializedInv.add(ser);
				}
			} else {
				serializedInv.add(new ItemInventorySerialized(null,0,i));
			}
		}
	}
	
	public static class ItemInventorySerialized {
		public String item;
		public int slot;
		public int amount;
		public ItemInventorySerialized() {}
		public ItemInventorySerialized(String data, int amount, int slot) {
			this.slot = slot;
			this.item = data;
			this.amount = amount;
		}
	}

}
