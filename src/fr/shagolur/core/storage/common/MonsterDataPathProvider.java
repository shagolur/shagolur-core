package fr.shagolur.core.storage.common;

import fr.shagolur.core.entities.MonsterData;
import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.Serializer;

import java.util.Arrays;

/**
 * PathProvider for a MonsterData.
 * To obtain the path, can give an Monster id or a MonsterData instance.
 * @see PathProvider
 * @see MonsterData
 */
public class MonsterDataPathProvider implements PathProvider<MonsterData> {
	
	public static final String PATH = "core/mobs/";
	
	@Override
	public Class<MonsterData> getHandledClass() {
		return MonsterData.class;
	}
	
	@Override
	public Class<? extends Serializer<MonsterData>> getSerializerClass() {
		return MonsterData.MonsterDataBDD.class;
	}
	
	@Override
	public String getPath(Object... params) {
		if(params.length == 0)
			return PATH;
		if(params.length != 1)
			throw new RuntimeException("PlayerPathProvider only allow 1 parameter : MonsterData/String. Provided " + Arrays.toString(params));
		if(params[0] instanceof String)
			return PATH + params[0] + "/";
		if(params[0] instanceof MonsterData)
			return PATH + ((MonsterData)params[0]).getId() + "/";
		throw new RuntimeException("PlayerPathProvider only allow 1 parameter : MonsterData/String. Provided " + params[0]);
	}
}
