package fr.shagolur.core.storage.common;

import com.google.gson.annotations.SerializedName;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.storage.Serializer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.util.UUID;

public class SerializerSPlayer implements Serializer<SPlayer> {
	
	public String uuid;
	public String nickname;
	public int money;
	@SerializedName("xp") public long exp;
	@SerializedName("current_health") public double bufferHealth;
	@SerializedName("current_mana") public double bufferMana;
	@SerializedName("playerinventory_set") public BufferInventory.ItemInventorySerialized[] inventoryItems;
	
	@SerializedName("spawn_world") public String spawnWorld;
	@SerializedName("spawn_x") public double spawnX;
	@SerializedName("spawn_y") public double spawnY;
	@SerializedName("spawn_z") public double spawnZ;
	//@SerializedName("playerequipment_set") public BufferInventory.ItemArmorSerialized[] armorItems;
	
	public SerializerSPlayer() {}
	
	public SerializerSPlayer(SPlayer player) {
		this.uuid = player.getUUID().toString();
		this.exp = player.getExp();
		this.nickname = player.getNickname();
		this.bufferHealth = player.getHealth();
		this.bufferMana = player.getMana();
		this.money = player.getMoney();
		inventoryItems = player.getBufferInventory().toInventorySerialized();
	}
	
	@Override
	public SPlayer deserialize() {
		Location loc = null;
		World world = Bukkit.getWorld(spawnWorld);
		if(world != null) {
			loc = new Location(world, spawnX, spawnY, spawnZ);
		}
		BufferInventory bi = new BufferInventory(inventoryItems);
		return new SPlayer(UUID.fromString(uuid), nickname, exp, bufferHealth, bufferMana, true, bi, loc, money);
	}
	
}
