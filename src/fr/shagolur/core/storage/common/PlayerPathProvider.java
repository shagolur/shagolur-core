package fr.shagolur.core.storage.common;

import fr.shagolur.core.players.SPlayer;
import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.Serializer;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.UUID;

/**
 * PathProvider for a SPlayer.
 * To obtain the path, can give an UUID, a SPlayer or a Player entity as an argument.
 * @see PathProvider
 * @see SPlayer
 */
public class PlayerPathProvider implements PathProvider<SPlayer> {
	
	public static final String PATH = "core/players/";
	
	@Override
	public Class<SPlayer> getHandledClass() {
		return SPlayer.class;
	}
	
	@Override
	public Class<? extends Serializer<SPlayer>> getSerializerClass() {
		return SerializerSPlayer.class;
	}
	
	@Override
	public String getPath(Object... params) {
		if(params.length == 0)
			return PATH;
		if(params.length != 1)
			throw new RuntimeException("PlayerPathProvider only allow 1 parameter : Player/SPlayer/UUID. Provided " + Arrays.toString(params));
		if(params[0] instanceof UUID)
			return PATH + params[0];
		if(params[0] instanceof Player)
			return PATH + ((Player)params[0]).getUniqueId() + "/";
		if(params[0] instanceof SPlayer)
			return PATH + ((SPlayer)params[0]).getUUID() + "/";
		throw new RuntimeException("PlayerPathProvider only allow 1 parameter : Player/SPlayer/UUID. Provided " + params[0]);
	}
}
