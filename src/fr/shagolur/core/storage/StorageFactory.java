package fr.shagolur.core.storage;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.storage.api.APIStorage;
import fr.shagolur.core.storage.empty.EmptyStorage;
import org.bukkit.configuration.ConfigurationSection;

import javax.annotation.Nullable;

public final class StorageFactory {
	private StorageFactory() {}
	
	private static final String MODE_API = "api";
	private static final String MODE_SQLITE = "sqlite";
	private static final String MODE_YAML = "yaml";
	private static final String MODE_NONE = "none";
	
	public static @Nullable StorageSystem factory(@Nullable ConfigurationSection config) {
		if(config == null) {
			Shagolur.error("No key 'storage' in CORE configuration.");
			return null;
		}
		String mode = config.getString("mode");
		if(mode == null) {
			Shagolur.error("No key specified for 'storage.mode' specified.");
			return null;
		}
		switch (mode) {
			case MODE_NONE -> {
				Shagolur.warning("Using EMPTY storage system.");
				return new EmptyStorage();
			}
			case MODE_API ->  {
				String apiUrl = config.getString("api.url");
				String apiToken = config.getString("api.token");
				if(apiUrl == null) {
					Shagolur.error("The mode is \""+MODE_API+"\", but no key 'storage.api.url' has been specified.");
					return null;
				}
				if(!apiUrl.endsWith("/"))
					apiUrl += "/";
				if(apiToken == null) {
					Shagolur.warning("The key 'storage.api.token' is not defined. Please, put an empty string to avoid mistakes.");
					apiToken = "";
				}
				Shagolur.log("Storage mode set to API, toward ["+apiUrl+"]. Has a token ? "+(!apiToken.isEmpty())+".");
				return new APIStorage(apiUrl, apiToken);
			}
			case MODE_SQLITE, MODE_YAML -> {
				Shagolur.error("This mode has not been implemented yet.");
				return null;
			}
			default -> {
				Shagolur.error("Unkown value in config for key 'storage.mode' : \""+mode+"\"." +
						"Allowed values: [\""+MODE_API+"\",\""+MODE_SQLITE+"\",\""+MODE_YAML+"\"].");
				return null;
			}
		}
	}
}
