package fr.shagolur.core.parties;

import fr.shagolur.core.common.Identifiable;
import fr.shagolur.core.players.SPlayer;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class Party implements Identifiable {
	
	private final UUID partyID;
	
	private UUID owner;
	private final Set<SPlayer> players = new HashSet<>();
	
	public Party(SPlayer player) {
		partyID = UUID.randomUUID();
		players.add(player);
		owner = player.getUUID();
	}
	
	public boolean isOwner(@NotNull SPlayer player) {
		return isOwner(player.getUUID());
	}
	public boolean isOwner(UUID uuid) {
		return Objects.equals(uuid, owner);
	}
	
	public void add(@NotNull SPlayer player) {
		this.players.add(player);
	}
	
	public void remove(@NotNull SPlayer player) {
		remove(player.getUUID());
	}
	public void remove(UUID uuid) {
		this.players.removeIf(p -> p.getUUID().equals(uuid));
		if(isOwner(uuid)) {
			owner = players.stream().map(SPlayer::getUUID).findAny().orElse(null);
		}
	}
	public boolean contains(@NotNull SPlayer player) {
		return players.contains(player);
	}
	public boolean contains(UUID uuid) {
		return players.stream().anyMatch(p -> p.getUUID().equals(uuid));
	}
	
	public void clear() {
		players.clear();
		owner = null;
	}
	
	public boolean isEmpty() {
		return players.isEmpty();
	}
	
	@Override
	public UUID getUUID() {
		return partyID;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		Party party = (Party) o;
		return Objects.equals(partyID, party.partyID);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(partyID);
	}
}
