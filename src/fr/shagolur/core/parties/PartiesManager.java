package fr.shagolur.core.parties;

import fr.shagolur.core.players.SPlayer;
import org.jetbrains.annotations.NotNull;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

public class PartiesManager {
	
	private final Set<Party> parties = new HashSet<>();
	
	public Party create(@NotNull SPlayer player) {
		removeFromParty(player);
		return new Party(player);
	}
	
	public void removeFromParty(@NotNull SPlayer player) {
		Party currentParty = getParty(player);
		if(currentParty == null)
			return;
		currentParty.remove(player);
		if(currentParty.isEmpty())
			parties.remove(currentParty);
	}
	
	public boolean areInSameParty(SPlayer a, SPlayer b) {
		Party pa = getParty(a);
		if(pa != null)
			return pa.equals(getParty(b));
		return false;
	}
	
	public Party getParty(@NotNull SPlayer player) {
		return getParty(player.getUUID());
	}
	public boolean hasParty(@NotNull SPlayer player) {
		return getParty(player) != null;
	}
	public Party getParty(UUID uuid) {
		return parties.stream().filter(p -> p.contains(uuid)).findFirst().orElse(null);
	}
	public boolean hasParty(UUID uuid) {
		return getParty(uuid) != null;
	}
	
}
