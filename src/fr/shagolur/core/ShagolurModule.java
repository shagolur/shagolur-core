package fr.shagolur.core;

import org.apache.commons.lang.NotImplementedException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;
import org.jetbrains.annotations.NotNull;

/**
 * A module of Shagolùr.
 */
public abstract class ShagolurModule extends JavaPlugin {
	
	public static ShagolurModule module() {
		throw new NotImplementedException("Please, implement this static method.");
	}
	
	/**
	 * Get the default file configuration.
	 * @return the main shagolurinstance config.
	 */
	@Override
	public @NotNull FileConfiguration getConfig() {
		return Shagolur.plugin().getConfig();
	}
	
	/**
	 * Actually do nothing.
	 */
	@Override
	public void reloadConfig() {}
	
	/**
	 * Actually do nothing.
	 */
	@Override
	public void saveConfig() {}
	
	/**
	 * Actually do nothing.
	 */
	@Override
	public void saveDefaultConfig() {}

}
