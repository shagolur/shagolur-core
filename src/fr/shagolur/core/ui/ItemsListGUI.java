package fr.shagolur.core.ui;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.items.ItemData;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class ItemsListGUI extends GiveListGUI<ItemData> {
	
	public ItemsListGUI(Player target) {
		super(ChatColor.BLUE + "" + ChatColor.BOLD + "Liste des items", target);
	}
	
	@Override
	protected List<ItemData> supply() {
		return Shagolur.getItemsManager().getStream().toList();
	}
	
}
