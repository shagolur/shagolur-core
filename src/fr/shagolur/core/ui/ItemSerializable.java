package fr.shagolur.core.ui;

import org.bukkit.inventory.ItemStack;

public interface ItemSerializable {
	
	default ItemStack toItemStack() {
		return toItemStack(1);
	}
	
	ItemStack toItemStack(int amount);
	
}
