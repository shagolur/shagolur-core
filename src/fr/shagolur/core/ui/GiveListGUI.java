package fr.shagolur.core.ui;

import fr.shagolur.core.utils.ItemBuilder;
import fr.shagolur.core.utils.MenuGUI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import java.util.List;

public abstract class GiveListGUI<T extends ItemSerializable> extends MenuGUI {
	
	private static final int AMOUNT_PER_PAGE = 9*5;
	private static final int SLOT_PREVIOUS = 9*5+2;
	private static final int SLOT_QUIT = 9*5+4;
	private static final int SLOT_NEXT = 9*5+6;
	
	private final List<T> items;
	private final int maxPages;
	private int page;
	private int lastThisPage;
	
	public GiveListGUI(String title, Player target) {
		super(title, 9*6, false);
		items = supply();
		maxPages = items.size() / AMOUNT_PER_PAGE;
		displayPage(1);
		show(target);
	}
	
	protected abstract List<T> supply();
	
	@Override
	public void onClick(InventoryClickEvent e) {
		e.setCancelled(true);
		switch (e.getSlot()) {
			case SLOT_QUIT -> e.getWhoClicked().closeInventory();
			case SLOT_PREVIOUS -> {if(page>1) displayPage(page-1);}
			case SLOT_NEXT -> {if(page<maxPages) displayPage(page+1);}
		}
		if(e.getSlot() > lastThisPage)
			return;
		assert e.getCurrentItem() != null;
		player.getPlayer().getInventory().addItem(e.getCurrentItem());
	}
	
	private void displayPage(int page) {
		this.page = page;
		int start = AMOUNT_PER_PAGE*(page-1);
		for(int i = 0; i < AMOUNT_PER_PAGE; i++) {
			if(start+i < items.size()) {
				addOption(items.get(start + i).toItemStack(), i);
				lastThisPage = i;
			} else {
				addOption(FILLER, i);
			}
		}
		for(int i = AMOUNT_PER_PAGE; i < getSize(); i++) {
			addOption(FILLER_DARK, i);
		}
		if(page > 1)
			addOption(new ItemBuilder(Material.ARROW).setDisplayName(ChatColor.YELLOW+"<-").build(), SLOT_PREVIOUS);
		if(page < maxPages)
			addOption(new ItemBuilder(Material.ARROW).setDisplayName(ChatColor.YELLOW+"->").build(), SLOT_NEXT);
		addOption(new ItemBuilder(Material.BARRIER).setDisplayName(ChatColor.RED+"Retour").build(), SLOT_QUIT);
	}
	
	@Override
	public void onClose(InventoryCloseEvent e) {
		removeFromList();
	}
}
