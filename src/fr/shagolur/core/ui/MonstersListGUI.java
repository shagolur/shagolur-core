package fr.shagolur.core.ui;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.entities.MonsterData;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class MonstersListGUI extends GiveListGUI<MonsterData> {
	
	public MonstersListGUI(Player target) {
		super(ChatColor.BLUE + "" + ChatColor.BOLD + "Liste des mobs", target);
	}
	
	@Override
	protected List<MonsterData> supply() {
		return Shagolur.getEntitiesManager().getStream().toList();
	}
	
}
