package fr.shagolur.core.entities;

import fr.shagolur.core.items.Data;
import fr.shagolur.core.items.DataModifier;

public class BasicStatsHolder implements StatsHolder {
	
	private double mh, mm, pd, md, pr, mr, lck;
	private int lvl;
	
	public BasicStatsHolder() {}
	
	public BasicStatsHolder(DataModifier.DataModifierSerializer[] array) {
		if(array == null)
			return;
		for(DataModifier.DataModifierSerializer s : array) {
			Data stat = Data.valueOf(s.stat);
			switch (stat) {
				case INVALID, BONUS_KB_RESIS, BONUS_ATTACK_SPEED, MOVEMENT_SPEED -> {} //TODO
				case PHYSICAL_DMG -> setPhysicalDamage(s.amount);
				case MAGIC_DMG -> setMagicalDamage(s.amount);
				case PHYSICAL_RES -> setPhysicalResistance(s.amount);
				case MAGIC_RES -> setMagicalResistance(s.amount);
				case BONUS_HEALTH -> setMaxHealth(s.amount);
				case BONUS_MANA -> setMaxMana(s.amount);
				case LUCK -> setLuck(s.amount);
			}
		}
	}
	
	public void setMaxHealth(double val) {
		this.mh = val;
	}
	
	public void setMaxMana(double val) {
		this.mm = val;
	}
	
	public void setPhysicalDamage(double val) {
		this.pd = val;
	}
	
	public void setMagicalDamage(double val) {
		this.md = val;
	}
	
	public void setPhysicalResistance(double val) {
		this.pr = val;
	}
	
	public void setMagicalResistance(double val) {
		this.mr = val;
	}
	
	public void setLuck(double val) {
		this.lck = val;
	}
	
	public void setLevel(int val) {
		this.lvl = val;
	}
	
	@Override
	public double getMaxHealth() {
		return mh;
	}
	
	@Override
	public double getMaxMana() {
		return mm;
	}
	
	@Override
	public double getPhysicalDamage() {
		return pd;
	}
	
	@Override
	public double getMagicalDamage() {
		return md;
	}
	
	@Override
	public double getPhysicalResistance() {
		return pr;
	}
	
	@Override
	public double getMagicalResistance() {
		return mr;
	}
	
	@Override
	public double getLuck() {
		return lck;
	}
	
	@Override
	public int getLevel() {
		return lvl;
	}
	
	@Override
	public StatsHolder clone() {
		try {
			return (StatsHolder) super.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
			return null;
		}/*
		BasicStatsHolder holder = new BasicStatsHolder();
		holder.mh = mh;
		holder.mm = mm;
		holder.pd = pd;
		holder.md = md;
		holder.pr = pr;
		holder.mr = mr;
		holder.lck = lck;
		holder.lvl = lvl;
		return holder;*/
	}
	
}
