package fr.shagolur.core.entities;

import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface Lootable {
	List<ItemStack> loot(double chance);
}
