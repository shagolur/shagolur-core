package fr.shagolur.core.entities;

import com.google.gson.annotations.SerializedName;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.items.DataModifier;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.items.Rarity;
import fr.shagolur.core.storage.Serializer;
import fr.shagolur.core.storage.Storable;
import fr.shagolur.core.ui.ItemSerializable;
import fr.shagolur.core.utils.ItemBuilder;
import fr.shagolur.core.utils.SerializationUtils;
import fr.shagolur.core.utils.StupidSerializer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.NamespacedKey;
import org.bukkit.entity.Ageable;
import org.bukkit.entity.Breedable;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.persistence.PersistentDataType;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Instantiate it from the {@link EntitiesManager#spawn}
 */
public class MonsterData implements Storable, ItemSerializable {
	
	public static NamespacedKey keyContainsMob() {
		return Shagolur.key("spawn-mob");
	}
	
	private final String id;
	
	private final String displayName;
	private final EntityType entityType;
	private final StatsHolder stats;
	
	private final long expDrop;
	protected final LootTable loots;
	protected final Map<EquipmentSlot, ItemData> equipment;
	protected boolean invisible;
	protected boolean babie;
	
	public MonsterData(String id, String displayName, EntityType entityType, long expDrop, StatsHolder stats, LootTable loots, Map<EquipmentSlot, ItemData> equipment) {
		this.id = id;
		this.entityType = entityType;
		this.displayName = displayName;
		this.expDrop = expDrop;
		this.loots = loots;
		this.equipment = equipment;
		this.stats = stats;
	}
	
	// package-protected to be created from EntitiesManager !
	SEntity spawn(Location location, @Nullable SEntity owner) {
		LivingEntity entity = (LivingEntity) location.getWorld().spawnEntity(location, entityType, CreatureSpawnEvent.SpawnReason.CUSTOM, et -> {
			LivingEntity e = (LivingEntity) et;
			e.setCustomNameVisible(true);
			e.setCustomName(displayName);
			e.setInvisible(invisible);
			if(e instanceof Ageable) {
				if(babie)
					((Ageable)e).setBaby();
				else
					((Ageable)e).setAdult();
				if(e instanceof Breedable)
					((Breedable)e).setAgeLock(true);
			}
			e.setCanPickupItems(false);
		});
		return new SEntity(entity, SEntityType.MOB, this, owner);
	}
	
	public String getId() {
		return id;
	}
	
	public String getDisplayName() {
		return displayName;
	}
	
	public EntityType getEntityType() {
		return entityType;
	}
	
	public StatsHolder getStats() {
		return stats;
	}
	
	public long getExpDrop() {
		return expDrop;
	}
	
	public LootTable getLoots() {
		return loots;
	}
	
	public void setBabie() {
		babie = true;
	}
	public void setInvisible() {
		invisible = true;
	}
	public boolean isInvisible() {
		return invisible;
	}
	public boolean isBabie() {
		return babie;
	}
	
	public Map<EquipmentSlot, ItemData> getEquipment() {
		return equipment;
	}
	
	@Override
	public Object storageID() {
		return id;
	}
	
	@Override
	public ItemStack toItemStack(int amount) {
		return new ItemBuilder(StupidSerializer.getEggForEntity(entityType), amount)
				.setDisplayName(Rarity.DEV.color + "[SPAWN]"+ChatColor.WHITE+" - " + displayName)
				.addLoreLines(ChatColor.DARK_GRAY+"Fait apparaître le mob spécifié.")
				.addItemFlag(ItemFlag.HIDE_ATTRIBUTES)
				.setPersistentData(keyContainsMob(), PersistentDataType.STRING, id)
				.build();
	}
	
	@Override
	public Serializer<?> serialize() {
		return new MonsterDataBDD(this);
	}
	
	@Override
	public boolean alreadyInBDD() {
		return true;
	}
	
	@Override
	public void declarePersistentInBDD() {
		Shagolur.error("No way to create items from game.");
	}
	
	public static class MonsterDataBDD implements Serializer<MonsterData> {
		
		@SerializedName("identifier") public String id;
		@SerializedName("display_name") public String name;
		@SerializedName("entity_type") public String type;
		@SerializedName("mobstat_set") public DataModifier.DataModifierSerializer[] stats;
		@SerializedName("exp_drop") public long expDrop;
		@SerializedName("mobloottable_set") public LootTable.LootElementSerialized[] loots;
		@SerializedName("mobequipment_set") public SEntityEquipment.EquipementSerialized[] equipment;
		public String flags;
		
		public MonsterDataBDD() {}
		public MonsterDataBDD(MonsterData data) {
			this.id = data.id;
			this.name = data.displayName;
			this.type = data.entityType.name();
			this.expDrop = data.expDrop;
			this.stats = data.stats.serialize();
			this.loots = data.loots.serialize();
			this.equipment = (SEntityEquipment.EquipementSerialized[]) data.equipment.entrySet()
					.stream()
					.map(SEntityEquipment.EquipementSerialized::new)
					.toArray();
			this.flags = SerializationUtils.extractFlags(data);
		}
		
		@Override
		public MonsterData deserialize() {
			Shagolur.debug(toString());
			// Entity type
			EntityType type;
			try {
				type = EntityType.valueOf(this.type);
			} catch(IllegalArgumentException ignored) {
				Shagolur.error("MonsterData [id="+id+"] as a bad entityType : \""+this.type+"\"");
				type = EntityType.ZOMBIE;
			}
			// Display name
			String displayName = ChatColor.translateAlternateColorCodes('&', name);
			// Stats
			StatsHolder stats = new BasicStatsHolder(this.stats);//SerializationUtils.stringToStats(this.stats);
			// Loots
			LootTable loots = new LootTable(id, this.loots);
			// Equipement
			Map<EquipmentSlot, ItemData> equipment = Stream.of(this.equipment)
					.filter(eqp -> eqp.identifier != null)
					.map(SEntityEquipment.EquipementSerialized::serialize)
					.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
			MonsterData data = new MonsterData(id, displayName, type, expDrop, stats, loots, equipment);
			// Flags
			SerializationUtils.applyFlagsToMonsterData(flags, data);
			return data;
		}
		
		@Override
		public String toString() {
			return "MonsterDataBDD{" +
					"id='" + id + '\'' +
					", name='" + name + '\'' +
					", type='" + type + '\'' +
					", stats=" + Arrays.toString(stats) +
					", expDrop=" + expDrop +
					", loots='" + Arrays.toString(loots) + '\'' +
					", equipment=" + Arrays.toString(equipment) +
					", flags='" + flags + '\'' +
					'}';
		}
	}
}
