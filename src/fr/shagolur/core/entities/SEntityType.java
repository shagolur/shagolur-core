package fr.shagolur.core.entities;

public enum SEntityType {
	
	PLAYER,
	NPC,
	
	MOB,
	SUPER_MOB,
	BOSS,
	
	SPELL,
	PROJECTILE
	
}
