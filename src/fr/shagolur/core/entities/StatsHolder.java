package fr.shagolur.core.entities;

import fr.shagolur.core.items.Data;
import fr.shagolur.core.items.DataModifier;
import fr.shagolur.core.items.Modifier;

import java.util.ArrayList;
import java.util.List;

public interface StatsHolder extends Cloneable {
	
	default double getData(Data data) {
		return switch (data) {
			case PHYSICAL_DMG -> getPhysicalDamage();
			case MAGIC_DMG -> getMagicalDamage();
			case PHYSICAL_RES -> getPhysicalResistance();
			case MAGIC_RES -> getMagicalResistance();
			case BONUS_HEALTH -> getMaxHealth();
			case BONUS_MANA -> getMaxMana();
			case INVALID, MOVEMENT_SPEED, BONUS_ATTACK_SPEED, BONUS_KB_RESIS, LUCK -> 0;
		};
	}
	
	double getMaxHealth();
	double getMaxMana();
	
	double getPhysicalDamage();
	double getMagicalDamage();
	double getPhysicalResistance();
	double getMagicalResistance();
	
	double getLuck();
	
	int getLevel();
	
	default DataModifier.DataModifierSerializer[] serialize() {
		List<DataModifier.DataModifierSerializer> list = new ArrayList<>();
		for(Data dataType : Data.values()) {
			if(getData(dataType) != 0) {
				list.add(new DataModifier.DataModifierSerializer(
						new DataModifier(dataType, Modifier.ADD_FLAT, getData(dataType))
				));
			}
		}
		return list.toArray(new DataModifier.DataModifierSerializer[0]);
	}
	
	StatsHolder clone();
}
