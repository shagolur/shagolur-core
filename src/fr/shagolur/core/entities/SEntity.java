package fr.shagolur.core.entities;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.common.DataHolder;
import fr.shagolur.core.common.Identifiable;
import fr.shagolur.core.effects.Effect;
import fr.shagolur.core.effects.EffectsHolder;
import fr.shagolur.core.events.ShagolurEntityDieEvent;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.players.PlayerLoots;
import fr.shagolur.core.players.SPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

public class SEntity implements EffectsHolder, DataHolder, Identifiable {
	
	private final Map<String, Object> metadata = new HashMap<>();
	protected LivingEntity entity;
	protected final Lootable lootable;
	private final SEntityType type;
	private final long droppedExp;
	protected String displayName;
	private final SEntityEquipment equipment; // for mobs, it does NOT change stats.
	
	private final Set<Effect> currentEffects = new HashSet<>();
	
	protected boolean dead;
	//TODO lastdamage cause !!
	private SEntity lastDamager;
	private SEntity owner;
	
	protected double healthPerSecond;
	protected double health;
	
	protected StatsHolder stats;
	
	protected SEntity(SEntityType type) {
		this.type = type;
		lootable = new PlayerLoots();
		droppedExp = 0;
		equipment = new SEntityEquipment(this);
	}
	public SEntity(LivingEntity entity, SEntityType type, MonsterData data, @Nullable SEntity owner) {
		this.entity = entity;
		this.type = type;
		this.owner = owner;
		displayName = data.getDisplayName();
		droppedExp = data.getExpDrop();
		lootable = data.getLoots();
		stats = data.getStats().clone();
		health = getMaxHealth();
		// Equipment
		equipment = new SEntityEquipment(this);
		equipment.init(data.getEquipment());
		equipment.equip(entity);
	}
	
	public boolean exists() {
		return !(getEntity() == null || dead);
	}
	
	public String getName() {
		return displayName;
	}
	
	public Location getLocation() {
		return getEntity().getLocation();
	}
	public void teleport(Location loc) {
		if (exists()) {
			getEntity().teleport(loc);
		}
	}
	public void teleport(Entity entity) {
		if (exists()) {
			getEntity().teleport(entity);
		}
	}
	
	public boolean hasOwner() {
		return owner != null;
	}
	public boolean isOwner(@Nullable Entity e) {
		if(e == null)
			return false;
		return isOwner(e.getUniqueId());
	}
	public boolean isOwner(@Nullable SEntity e) {
		if(e == null)
			return false;
		return isOwner(e.getUUID());
	}
	public boolean isOwner(@NotNull UUID uuid) {
		if(getUUID().equals(uuid))
			return true;
		if(!hasOwner())
			return false;
		return owner.isOwner(uuid);
	}
	
	public boolean damageBy(@NotNull SEntity damager, double damages, boolean physical) {
		return internalDamage(damages, damager, physical);
	}
	public boolean damage(double damages, boolean physical) {
		return internalDamage(damages, null, physical);
	}
	
	public void heal(double amount) {
		if(isDead())
			return;
		health = Math.min(getMaxHealth(), health + amount);
		afterHealthChanged();
	}
	
	/**
	 * @return true IF the entity has been killed
	 */
	private boolean internalDamage(double amount, @Nullable SEntity damager, boolean isMagical) {
		if (!exists()) {
			return false;
		}
		double damage = Math.max(0, amount - getResistance(isMagical));
		if(damager != null)
			this.lastDamager = damager;
		health -= damage;
		if (health <= 0) {
			health = 0;
			die();
		} else {
			afterHealthChanged();
		}
		return dead;
	}
	
	protected void afterHealthChanged() {}
	
	public double getHealthRatio() {
		if (getMaxHealth() <= 0) {
			Shagolur.warning("MaxHealth == 0 pour entity " + getName());
			return 0;
		}
		return health / getMaxHealth();
	}
	
	public void die() {
		Location loc = entity.getLocation();
		currentEffects.clear();
		// Loots
		Bukkit.broadcastMessage("hop il meurt : " + getName());
		ShagolurEntityDieEvent deathEvent = new ShagolurEntityDieEvent(this, lastDamager);
		Bukkit.getPluginManager().callEvent(deathEvent);
		// Kill it
		dead = true;
		entity.damage(99999);
		// Split exp according to event repartition exp
		if(getDroppedExp() > 0) {
			for(Map.Entry<SEntity, Double> repartition : deathEvent.getPercentagesExp().entrySet()) {
				SEntity owner = repartition.getKey().getRootOwner();
				if(owner instanceof SPlayer && repartition.getValue() > 0) {
					((SPlayer)owner).addExp((long)( getDroppedExp() * repartition.getValue() ));
				}
			}
		}
		// Spawn loots on the ground
		for(ItemStack is : deathEvent.getLoots())
			getLocation().getWorld().dropItem(loc.add(0,0.2,0), is);
		// Delete from manager
		Shagolur.getEntitiesManager().reportDeadEntity(this);
	}
	
	public double getHealthRegen() {
		return healthPerSecond;
	}
	
	@Override
	public void tick(double elapsed) {
		if(isDead())
			return;
		currentEffects.forEach(e -> e.tick(elapsed));
		currentEffects.removeIf(Effect::isOver);
		health = Math.min(getMaxHealth(), health + getHealthRegen() * elapsed);
		afterHealthChanged();
	}
	
	public boolean isDead() {
		return dead;
	}
	
	public SEntity getRootOwner() {
		if(hasOwner())
			return owner.getRootOwner();
		return this;
	}
	
	public double getMaxHealth() {
		return stats.getMaxHealth();
	}
	
	public double getHealth() {
		return health;
	}
	
	public double getAttackPhysic() {
		return stats.getPhysicalDamage();
	}
	
	public double getAttackMagic() {
		return stats.getMagicalDamage();
	}
	
	public double getResistancePhysic() {
		return stats.getPhysicalResistance();
	}
	
	public double getResistanceMagic() {
		return stats.getMagicalResistance();
	}
	
	public SEntity getLastDamager() {
		return lastDamager;
	}
	
	public SEntityType getCategory() {
		return type;
	}
	
	@Override
	public UUID getUUID() {
		return getEntity().getUniqueId();
	}
	
	public LivingEntity getEntity() {
		return entity;
	}
	
	public Lootable loots() {
		return lootable;
	}
	
	public long getDroppedExp() {
		return droppedExp;
	}
	
	public @Nullable EntityEquipment getBukkitEquipment() {
		if(!exists())
			return null;
		return getEntity().getEquipment();
	}
	
	public @NotNull SEntityEquipment getEquipment() {
		return equipment;
	}
	
	public double getResistance(boolean isMagical) {
		return isMagical ? getResistanceMagic() : getResistancePhysic();
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		return ((SEntity)o).getUUID().equals(getUUID());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(getUUID());
	}
	
	public double getAttackValue() {
		if(isAttackMagical())
			return getAttackMagic();
		return getAttackPhysic();
	}
	
	public boolean isAttackMagical() {
		ItemData mainWeapon = getEquipment().getMainWeapon();
		if(mainWeapon == null)
			return false;
		return mainWeapon.isMagical();
	}
	
	@Override
	public boolean hasEffect(String identifier) {
		return currentEffects.stream().anyMatch(e -> e.getIdentifier().equals(identifier));
	}
	
	@Override
	public void addEffect(String identifier, SEntity source, double duration, double force) {
		currentEffects.add(
			Shagolur.getEffectsManager().create(identifier, this, source, duration, 1)
		);
	}
	
	@Override
	public void addEffect(Effect effect) {
		currentEffects.add(effect);
	}
	
	@Override
	public void setData(@NotNull String key, @NotNull Object data) {
		metadata.put(key, data);
	}
	
	@Override
	public <T> T getData(@NotNull String key, @NotNull Class<T> clazz) throws ClassCastException {
		if( ! metadata.containsKey(key))
			return null;
		return clazz.cast(metadata.get(key));
	}
	
	public void setHealth(double val) {
		this.health = Math.max(0, Math.min(getMaxHealth(), val));
	}
}