package fr.shagolur.core.entities;

import com.google.gson.annotations.SerializedName;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class LootTable implements Lootable {
	
	private final Map<ItemData, LootStats> loots = new HashMap<>();
	
	public LootTable(Map<String, LootStats> loots) {
		for(String key : loots.keySet()) {
			this.loots.put(Shagolur.getItemsManager().get(key), loots.get(key));
		}
	}
	public LootTable(String debugMobId, LootElementSerialized[] lootElementSerializeds) {
		for(LootElementSerialized les : lootElementSerializeds) {
			ItemData item = Shagolur.getItemsManager().get(les.item);
			if(item == null) {
				Shagolur.error("Unknown item in loot table of monster ["+debugMobId+"] : \""+les.item+"\".");
			}
			loots.put(item, new LootStats(les.chance/100, les.min, les.max));
		}
	}
	
	public boolean isEmpty() {
		return loots.isEmpty();
	}
	
	public void put(ItemData item, LootStats stats) {
		loots.put(item, stats);
	}
	public void put(ItemData item, double dropChance, int minDrop, int maxDrop) {
		loots.put(item, new LootStats(dropChance, minDrop, maxDrop));
	}
	
	public Set<Map.Entry<String, LootStats>> getSerializableEntries() {
		return loots.entrySet().stream()
				.map(en -> Map.entry(en.getKey().getId(), en.getValue()))
				.collect(Collectors.toSet());
	}
	
	public List<ItemStack> loot(double chance) {
		Bukkit.broadcastMessage("DROP witch luck="+chance);
		List<ItemStack> drops = new LinkedList<>();
		for(Map.Entry<ItemData,LootStats> loot : loots.entrySet()) {
			double rand = Math.random() / chance;
			if(loot.getValue().dropChance() >= rand) {
				int amount = Utils.randInt(Math.random()/chance, loot.getValue().amountMin(), loot.getValue().amountMax());
				if(amount < 1)
					amount = 1;
				drops.add(loot.getKey().toItemStack(amount));
			}
		}
		return drops;
	}
	
	public static record LootStats(
			double dropChance,
			int amountMin,
			int amountMax
	) {
		@Override
		public String toString() {
			return "{" + (dropChance*100.0) + "%, x"+(amountMax==amountMin?""+amountMin:"["+amountMin+","+amountMax+"]")+"}";
		}
	}
	
	public LootElementSerialized[] serialize() {
		return (LootElementSerialized[]) loots.entrySet().stream().map(LootElementSerialized::new).toArray();
	}
	
	public static class LootElementSerialized {
		public String item;
		@SerializedName("min_amount") public int min;
		@SerializedName("max_amount") public int max;
		public double chance;
		public LootElementSerialized() {}
		public LootElementSerialized(Map.Entry<ItemData, LootStats> entry) {
			item = entry.getKey().getId();
			min = entry.getValue().amountMin;
			max = entry.getValue().amountMax;
			chance = entry.getValue().dropChance;
		}
		
		@Override
		public String toString() {
			return "{'" + item + '\'' +
					", [" + min + "," + max +
					"], %=" + chance +
					'}';
		}
	}
}
