package fr.shagolur.core.entities;

import fr.shagolur.core.Shagolur;
import fr.shagolur.core.common.Loopable;
import fr.shagolur.core.common.Purgeable;
import fr.shagolur.core.players.PlayersManager;
import fr.shagolur.core.storage.PathProvider;
import fr.shagolur.core.storage.StorableManager;
import fr.shagolur.core.storage.common.MonsterDataPathProvider;
import fr.shagolur.core.utils.Loop;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class EntitiesManager extends StorableManager<MonsterData> implements Purgeable, Loopable {
	
	private final Map<UUID, SEntity> entities = new HashMap<>();
	private final Loop loop;
	
	public EntitiesManager() {
		loop = new Loop(this::tick, 20L);
	}
	
	@Override
	protected PathProvider<MonsterData> getProvider() {
		return new MonsterDataPathProvider();
	}
	
	@Override
	protected String getStoredTypeName() {
		return "monsters";
	}
	
	public @Nullable SEntity spawn(String dataID, @NotNull Location location) {
		MonsterData data = get(dataID);
		if(data == null)
			return null;
		return spawn(data, location, null);
	}
	public @NotNull SEntity spawn(@NotNull MonsterData data, @NotNull Location location) {
		return spawn(data, location, null);
	}
	public @NotNull SEntity spawn(@NotNull MonsterData data, @NotNull Location location, @Nullable SEntity owner) {
		SEntity entity = data.spawn(location, owner);
		entities.put(entity.getUUID(), entity);
		return entity;
	}
	
	public @Nullable SEntity getSEntity(@NotNull UUID uuid) {
		SEntity en = entities.get(uuid);
		if(en == null)
			return PlayersManager.uuidIsPlayer(uuid) ? Shagolur.getPlayersManager().getPlayer(uuid) : null;
		return en;
	}
	public @Nullable SEntity getSEntity(@NotNull Entity en) {
		return getSEntity(en.getUniqueId());
	}
	public @Nullable SEntity getSEntity(@NotNull Player p) {
		return Shagolur.getPlayersManager().getPlayer(p);
	}
	
	@Override
	public void startLoop() {
		loop.startLoop();
	}
	
	@Override
	public void stopLoop() {
		loop.stopLoop();
	}
	
	private void tick(double elapsed) {
		entities.values().forEach(e -> e.tick(elapsed));
	}
	
	public void purge() {
		Shagolur.log("Purging entities.");
		for(SEntity entity : entities.values()) {
			if(entity.exists()) {
				entity.getEntity().remove();
			}
		}
		entities.clear();
	}
	
	public void reportDeadEntity(SEntity sEntity) {
		if(sEntity.exists()) {
			Shagolur.warning("Something tried to report as dead this entity, but it was still alive : " + sEntity.getName());
			return;
		}
		entities.remove(sEntity.getUUID());
	}
}
