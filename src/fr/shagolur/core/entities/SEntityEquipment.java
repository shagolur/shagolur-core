package fr.shagolur.core.entities;

import com.google.gson.annotations.SerializedName;
import fr.shagolur.core.Shagolur;
import fr.shagolur.core.items.DataModifier;
import fr.shagolur.core.items.ItemData;
import fr.shagolur.core.items.Rarity;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class SEntityEquipment {
	
	private final Map<EquipmentSlot, ItemData> equipment = new HashMap<>();
	private final SEntity entity;
	
	public SEntityEquipment(SEntity entity) {
		this.entity = entity;
		updateData();
	}
	
	public void updateData() {
		EntityEquipment equipment = entity.getBukkitEquipment();
		if(equipment == null)
			return;
		Shagolur.log("SEntityEquipment#updateData ("+entity.getName()+").");
		for(EquipmentSlot slot : EquipmentSlot.values()) {
			ItemData data = ItemData.convert(equipment.getItem(slot));
			this.equipment.put(slot, data);
			if(data != null)
				Shagolur.debug("-> " + slot + " : " + data);
		}
	}
	
	public void updatePart(EquipmentSlot slot, ItemData data) {
		this.equipment.put(slot, data);
	}
	
	public @Nullable ItemData getItem(EquipmentSlot slot) {
		return equipment.getOrDefault(slot, null);
	}
	public @Nullable ItemData getMainWeapon() {
		return getItem(EquipmentSlot.HAND);
	}
	public @Nullable ItemData getOffHandWeapon() {
		return getItem(EquipmentSlot.OFF_HAND);
	}
	public @Nullable ItemData getHelmet() {
		return getItem(EquipmentSlot.HEAD);
	}
	public @Nullable ItemData getChestplate() {
		return getItem(EquipmentSlot.CHEST);
	}
	public @Nullable ItemData getLeggings() {
		return getItem(EquipmentSlot.LEGS);
	}
	public @Nullable ItemData getBoots() {
		return getItem(EquipmentSlot.FEET);
	}
	
	public void equip(LivingEntity entity) {
		if(entity.getEquipment() == null)
			return;
		for(Map.Entry<EquipmentSlot,ItemData> en : equipment.entrySet()) {
			ItemStack is = en.getValue() == null ? new ItemStack(Material.AIR) : en.getValue().toItemStack();
			switch (en.getKey()) {
				case HAND -> entity.getEquipment().setItemInMainHand(is);
				case OFF_HAND -> entity.getEquipment().setItemInOffHand(is);
				case HEAD -> entity.getEquipment().setHelmet(is);
				case CHEST -> entity.getEquipment().setChestplate(is);
				case LEGS -> entity.getEquipment().setLeggings(is);
				case FEET -> entity.getEquipment().setBoots(is);
			}
		}
	}
	
	public boolean isEmpty() {
		for(EquipmentSlot slot : EquipmentSlot.values()) {
			if(getItem(slot) != null)
				return false;
		}
		return true;
	}
	
	public Set<DataModifier> getAllModifiers() {
		Set<Set<DataModifier>> setSet = new HashSet<>();
		for(EquipmentSlot slot : EquipmentSlot.values()) {
			ItemData data = getItem(slot);
			if(data != null) {
				setSet.add(data.getModifiers());
			}
		}
		return DataModifier.flatten(setSet);
	}
	
	public String toString(String color) {
		if(isEmpty())
			return "§7(empty)";
		StringBuilder sb = new StringBuilder();
		for(EquipmentSlot slot : EquipmentSlot.values()) {
			ItemData data = getItem(slot);
			if(data != null) {
				sb.append(color)
						.append("\n - ")
						.append(slot.name())
						.append(" : ")
						.append(data.getName())
						.append(color)
						.append(" ")
						.append(data.stringStats(color));
			}
		}
		return sb.toString();
	}
	
	public void init(Map<EquipmentSlot, ItemData> equipment) {
		this.equipment.putAll(equipment);
	}
	
	public static class EquipementSerialized {
		@SerializedName("item") public String identifier;
		public String slot;
		
		public EquipementSerialized() {}
		public EquipementSerialized(Map.Entry<EquipmentSlot, ItemData> entry) {
			identifier = entry.getValue().getId();
			slot = entry.getKey().name();
		}
		
		public Map.Entry<EquipmentSlot, ItemData> serialize() {
			EquipmentSlot slot;
			try {
				if(this.slot == null)
					throw new IllegalArgumentException();
				slot = EquipmentSlot.valueOf(this.slot);
			} catch(IllegalArgumentException e) {
				slot = EquipmentSlot.OFF_HAND;
				Shagolur.error("Error with EquipementSerialized[item_id="+identifier+"] : bad equipmentslot value : \""+ this.slot + "\".");
			}
			ItemData data = Shagolur.getItemsManager().get(identifier);
			if(data == null) {
				Shagolur.error("Error with EquipementSerialized[item_id="+identifier+"] : is unknown !");
				return Map.entry(slot, new ItemData(identifier, Material.BARRIER, false, false, Rarity.COMMON, "brokenItem"));
			}
			return Map.entry(slot, data);
		}
		
		@Override
		public String toString() {
			return "EquipementSerialized{" +
					"identifier='" + identifier + '\'' +
					", slot='" + slot + '\'' +
					'}';
		}
	}
}
